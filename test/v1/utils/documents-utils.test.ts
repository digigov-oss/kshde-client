import { FILE_MEDIUM, FILE_SMALL, KSHDETestClient } from '../lib/testClient';
import fs from 'fs';
import { checkReceipt } from '../lib/receipt';
import { createDocumentPayload } from '../lib/utils';
import { checkAttachmentResult } from '../lib/attachment';

const documentUtils = KSHDETestClient.utils.documents;
const documents = KSHDETestClient.documents;

describe('documents-utils resource tests', () => {
    it(
        'should post a document with attachments and retrieve them and their contents by version',
        async () => {
            const attachmentsFiles = [FILE_SMALL, FILE_MEDIUM];
            const document = createDocumentPayload(fs.createReadStream(FILE_SMALL));
            const attachments = [
                {
                    data: fs.createReadStream(attachmentsFiles[0]),
                    metadata: { FileName: attachmentsFiles[0].split('/').pop() ?? '' }
                },
                {
                    data: fs.createReadStream(attachmentsFiles[1]),
                    metadata: { FileName: attachmentsFiles[1].split('/').pop() ?? '' }
                }
            ];
            const result = await documentUtils.postAll(document, attachments);
            const receipt = result.receipt;
            expect(receipt).toBeDefined();
            checkReceipt(receipt);
            const documentProtocolNo = receipt.DocumentProtocolNo;
            const attachmentsResults = result.attachments;
            attachmentsResults.forEach((attachmentResult, index) => {
                const isFinal = index === attachmentsResults.length - 1 ? true : false;
                checkAttachmentResult(attachmentResult, isFinal);
            });
            for (const [index, attachmentResult] of attachmentsResults.entries()) {
                const attachment = await documents(documentProtocolNo)
                    .attachments(attachmentResult.AttachmentId)
                    .get();
                expect(attachment).toBeDefined();
                checkAttachmentResult(attachmentResult, attachmentResult.IsFinal ?? false);
                // Give sometime to server to hadle saving the files
                await new Promise<void>((resolve) => {
                    setTimeout(() => {
                        resolve();
                    }, 5000);
                });
                const attachmentContent = await documents(documentProtocolNo)
                    .attachments(attachment.AttachmentId)
                    .content.get();
                const writeStream = fs.createWriteStream(
                    `/tmp/downloaded_attachment_${attachmentResult.FileName}`
                );
                attachmentContent.pipe(writeStream);
                await new Promise<void>((resolve) => {
                    writeStream.on('close', resolve);
                });
                expect(
                    fs.statSync(`/tmp/downloaded_attachment_${attachmentResult.FileName}`).size
                ).toEqual(fs.statSync(attachmentsFiles[index]).size);
            }
            const allAttachments = await documents(documentProtocolNo).attachments.get();
            expect(allAttachments).toBeDefined();
            expect(allAttachments.length).toEqual(2);
        },
        4 * 60 * 1000
    );
});
