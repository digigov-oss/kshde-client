import { KSHDETestClient } from '../lib/testClient';

const resource = KSHDETestClient.utils.orgchart;

describe('orgchart resource tests', () => {
    it('should return the orgchart tree as array', async () => {
        const organizations = await resource.toArray();
        expect(organizations).toBeDefined();
        expect(organizations.length).toBeGreaterThan(0);
    });
});
