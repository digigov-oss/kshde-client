import { invalidFileNames } from './isFilenameValid.test';
import { FILE_SMALL, KSHDETestClient } from '../lib/testClient';
import fs from 'fs';
import { createDocumentPayload } from '../lib/utils';
import { PostAttachmentMetadata } from '../../../src/v1/client/utils/documents-utils';
import { DocumentPayload } from '../../../src/v1';

const documentUtils = KSHDETestClient.utils.documents;

describe('documents-utils resource fail tests', () => {
    it('should fail to send documents with same name', async () => {
        const document = createDocumentPayload(fs.createReadStream(FILE_SMALL));
        const attachments: DocumentPayload<PostAttachmentMetadata>[] = [
            {
                data: fs.createReadStream(FILE_SMALL),
                metadata: { FileName: 'attachment.pdf' }
            },
            {
                data: fs.createReadStream(FILE_SMALL),
                metadata: { FileName: 'attachment.pdf' }
            }
        ];

        expect(async () => {
            await documentUtils.postAll(document, attachments);
        }).rejects.toThrow('FileName attachment.pdf is not unique');
    });

    it('should fail to send documents with zero recipients', async () => {
        const document = createDocumentPayload(fs.createReadStream(FILE_SMALL), 'test', []);
        expect(async () => {
            await documentUtils.postAll(document);
        }).rejects.toThrow('No RecipientSectorCodes given');
    });

    it('should fail to send documents with invalid file names', async () => {
        invalidFileNames.forEach((fileName) => {
            const document = createDocumentPayload(fs.createReadStream(FILE_SMALL), fileName);
            expect(async () => {
                await documentUtils.postAll(document);
            }).rejects.toThrow(
                `FileName ${fileName} contains special characters or starts or ends with dot or is longer than 255 characters`
            );
        });
    });
});
