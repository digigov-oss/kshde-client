import { FILE_SMALL } from '../lib/testClient';
import { Readable } from 'stream';
import fs from 'fs';
import { createDocumentPayload } from '../lib/document';
import axiosRetry, { IAxiosRetryConfig } from 'axios-retry';
import { AxiosError } from 'axios';
import { Config, KSHDEClient } from '../../../src/v1';

const expectedRetries = 3;

describe('prove that retry config is respected', () => {
    let KSHDETestClient: KSHDEClient;
    let retries = 0;
    beforeAll(() => {
        const retryConfig: IAxiosRetryConfig = {
            retries: expectedRetries,
            retryDelay: (...arg) => axiosRetry.exponentialDelay(...arg, 300),
            retryCondition(error: AxiosError) {
                if (error.response)
                    switch (error.response.status) {
                        //retry only if status is 401 just for this test
                        //this is not realistic for practical uses so change to your needs
                        case 401:
                            return true;
                        default:
                            return false;
                    }
                return false;
            },
            onRetry: (retryCount, error) => {
                retries = retryCount;
                console.log(`retry count: ${retryCount} ${error.response?.config.url}`);
            }
        };

        const config: Config = {
            clientId: 'BAD_CLENT_ID',
            clientSecret: process.env.KSHDE_CLIENT_SECRET ?? '',
            sectorCode: process.env.KSHDE_SECTOR_CODE ?? '',
            retryConfig: retryConfig
        };

        KSHDETestClient = new KSHDEClient(config);
    });

    it.only('should retry to authenticate as requested', async () => {
        const documentBuffer = fs.readFileSync(FILE_SMALL);
        const document = createDocumentPayload(Readable.from(documentBuffer));

        await KSHDETestClient.documents.post(document).catch(() => {
            expect(retries).toEqual(expectedRetries);
        });
    });
});
