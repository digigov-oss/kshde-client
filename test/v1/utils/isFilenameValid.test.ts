import { isFilenameValid } from '../../../src/v1/utils/validateFiles';

export const invalidFileNames = [
    '.document.pdf',
    'document.pdf.',
    'documen;t.pdf',
    'documen/t.pdf',
    'documen:t.pdf',
    'documen*t.pdf',
    'documen<t.pdf',
    'documen>t.pdf',
    'documen|t.pdf',
    'documen"t.pdf',
    'veryLongFileName'.repeat(20)
];

describe('validates filenames', () => {
    const validFileNames = ['do.c.u..me..nt.pdf'];
    it('should validate filenames according to SHDE rules', async () => {
        validFileNames.forEach((fileName) => {
            expect(isFilenameValid(fileName)).toBe(true);
        });
        invalidFileNames.forEach((fileName) => {
            expect(isFilenameValid(fileName)).toBe(false);
        });
    });
});
