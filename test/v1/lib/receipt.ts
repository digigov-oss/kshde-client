import { ReceiptApiResult } from '../../../src/v1/api/defs/receipt';

export function checkReceipt(receipt: ReceiptApiResult) {
    expect(receipt).toBeDefined();
    expect(receipt.ReceiptId).toBeDefined();
    expect(receipt.ReceiptDate).toBeDefined();
    expect(receipt.DocumentProtocolNo).toBeDefined();
    expect(receipt.VersionNumber).toBeDefined();
    expect(receipt.Type).toBeDefined();
    expect(receipt.OriginatorId).toBeDefined();
    expect(receipt.RecipientId).toBeDefined();
    expect(receipt.LocalReceiptId).toBeDefined();
    expect(receipt.Links).toBeDefined();
}
