import { Readable } from 'stream';
import {
    DocumentApiResult,
    DocumentCreateData,
    DocumentPayload,
    DocumentUpdateData
} from '../../../src/v1/api/defs/document';
import { RECIPIENT_CODE } from './testClient';

import { KSHDETestClient } from './testClient';

const documents = KSHDETestClient.documents;

export function createDocumentPayload(
    stream: Readable,
    fileName?: string,
    recipientSectorCodes = [1234567],
    isFinal = true
): DocumentPayload<DocumentCreateData> {
    return {
        data: stream,
        metadata: {
            Subject: 'subject',
            FileName: fileName ?? 'test.pdf',
            LocalSectorProtocolNo: '100',
            LocalSectorProtocolDate: new Date().toISOString(),
            RecipientSectorCodes: recipientSectorCodes,
            IsFinal: isFinal,
            OrgChartVersion: 1
        }
    };
}
export function createUpdateDocumentPayload(
    stream: Readable,
    recipientSectorCodes = [1234567]
): DocumentPayload<DocumentUpdateData> {
    return {
        data: stream,
        metadata: {
            Subject: 'version subject',
            VersionComments: 'These are the version comments',
            LocalSectorProtocolNo: '100',
            LocalSectorProtocolDate: new Date().toISOString(),
            RecipientSectorCodes: recipientSectorCodes,
            OrgChartVersion: 1,
            IsFinal: true
        }
    };
}

export async function updateDocument(
    stream: Readable,
    documentProtocolNo: string,
    doc: DocumentApiResult
) {
    return await documents(documentProtocolNo).put({
        data: stream,
        metadata: {
            Subject: doc.Subject,
            VersionComments: 'These are the version comments',
            LocalSectorProtocolNo: doc.LocalSectorProtocolNo,
            LocalSectorProtocolDate: doc.LocalSectorProtocolDate,
            RecipientSectorCodes: [RECIPIENT_CODE],
            IsFinal: true,
            OrgChartVersion: 1
        }
    });
}
