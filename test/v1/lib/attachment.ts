import { DocumentAttachmentApiResult } from '../../../src/v1';

export function checkAttachmentResult(receipt: DocumentAttachmentApiResult, isFinal: boolean) {
    expect(receipt.AttachmentId).toBeDefined();
    expect(receipt.DocumentProtocolNo).toBeDefined();
    expect(receipt.VersionNumber).toBeDefined();
    expect(receipt.FileName).toBeDefined();
    expect(receipt.Comments).toBeDefined();
    expect(receipt.Hash).toBeDefined();
    expect(receipt.DateReceived).toBeDefined();
    expect(receipt.Links).toBeDefined();
    expect(receipt.IsFinal).toBe(isFinal);
}
