import { Config, KSHDEClient } from '../../../src/v1';

export const FILE_SMALL = `${__dirname}/../../pdf/small.pdf`;
export const FILE_MEDIUM = `${__dirname}/../../pdf/medium.pdf`;
export const FILE_HUGE = `${__dirname}/../../pdf/huge.pdf`;
export const RECIPIENT_CODE = 1234567;

export const TEST_API_BASE_URL = 'https://sdddsp.mindigital-shde.gr/api/v1';

export const config: Config = {
    clientId: process.env.KSHDE_CLIENT_ID ?? '',
    clientSecret: process.env.KSHDE_CLIENT_SECRET ?? '',
    sectorCode: process.env.KSHDE_SECTOR_CODE ?? ''
};

export const KSHDETestClient = new KSHDEClient(config);
