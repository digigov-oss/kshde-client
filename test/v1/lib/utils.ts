import { Readable } from 'stream';
import { PostDocumentMetadata } from '../../../src/v1/client/utils/documents-utils';
import { DocumentPayload } from '../../../src/v1';

export function createDocumentPayload(
    stream: Readable,
    fileName?: string,
    recipientSectorCodes: number[] = [1234567]
): DocumentPayload<PostDocumentMetadata> {
    return {
        data: stream,
        metadata: {
            Subject: 'subject',
            FileName: fileName ?? 'test.pdf',
            LocalSectorProtocolNo: '100',
            LocalSectorProtocolDate: new Date().toISOString(),
            RecipientSectorCodes: recipientSectorCodes
        }
    };
}
