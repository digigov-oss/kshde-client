import { PagedApiResult } from '../../../src/v1/api/defs/paged-result';
import NavigatableApiResult from '../../../src/v1/api/utils/navigatable-api-result';

describe('Navigatable api resource tests', () => {
    it('should faile when called with invalid parameters', async () => {
        const get = async (): Promise<PagedApiResult<void>> => {
            return {
                results: [],
                NextPage: null,
                PreviousPage: null
            };
        };
        const instance = new NavigatableApiResult<void>(get);

        const invalidLimitValues = [200, -2];
        for (const invalidLimit of invalidLimitValues) {
            expect(async () => {
                await instance.init({
                    page: 1,
                    limit: invalidLimit,
                    params: {}
                });
            }).rejects.toThrow(`Limit ${invalidLimit} must be between 0 and 100`);
        }
        expect(async () => {
            await instance.init({
                page: -1,
                limit: 50
            });
        }).rejects.toThrow(`Page -1 must be greater than 0`);
    });
});
