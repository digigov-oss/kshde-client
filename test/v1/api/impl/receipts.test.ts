import { ReceiptApiResult } from '../../../../src/v1/api/defs/receipt';
import NavigatableApiResult from '../../../../src/v1/api/utils/navigatable-api-result';
import { checkReceipt } from '../../lib/receipt';
import { KSHDETestClient } from '../../lib/testClient';

const receipts = KSHDETestClient.receipts;
describe('receipts resource tests', () => {
    it('should retrieve receipts with no params', async () => {
        const result = await receipts.get();
        expect(result).toBeDefined();
        expect(result.data).toBeDefined();
        expect(result.data?.length).toBeLessThanOrEqual(50);
    });

    it('should retrieve and navigate receipts for different pages and limits', async () => {
        //Read page 2
        let results = await receipts.get({ page: 2, limit: 20 });
        checkReceipts(results, 20);
        const page2Ids = getIds(results);

        //Navigate to page 2
        results = await (await receipts.get({ page: 1, limit: 20 })).next();
        checkReceipts(results, 20);
        let pageIds = getIds(results);
        //Page 2 results should match
        expect(page2Ids).toEqual(pageIds);

        //Navigate to page 3
        results = await results.next();
        checkReceipts(results, 20);
        pageIds = getIds(results);

        //Page 2 results should not match
        expect(page2Ids).not.toEqual(pageIds);

        //Navigate to page 2 again
        results = await results.previous();
        checkReceipts(results, 20);
        pageIds = getIds(results);

        //Page 2 results should match
        expect(page2Ids).toEqual(pageIds);

        //Navigate to page 1
        results = await results.previous();
        checkReceipts(results, 20);
        pageIds = getIds(results);

        //Page 2 results should not match
        expect(page2Ids).not.toEqual(pageIds);

        //Navigate to page 0
        results = await results.previous();
        expect(results.data).toBe(undefined);
    });

    it('should be able to read all receipts', async () => {
        const results = await receipts.get({ limit: 100 });
        let total = 0;
        let data = results.data;
        while (data) {
            total += data.length ?? 0;
            if (total > 400) break;
            data = (await results.next()).data;
        }
        expect(total).toBeGreaterThan(100);
    });
});

function checkReceipts(receipts: NavigatableApiResult<ReceiptApiResult>, length: number) {
    expect(receipts).toBeDefined();
    expect(receipts.data).toBeDefined();
    expect(receipts.data?.length).toBe(length);
    receipts.data?.forEach((receipt) => checkReceipt(receipt));
}

function getIds(receipts: NavigatableApiResult<ReceiptApiResult>) {
    return receipts.data?.map((receipt) => receipt.ReceiptId);
}
