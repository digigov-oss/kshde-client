import { createDocumentPayload } from '../../lib/document';
import { FILE_SMALL, KSHDETestClient } from '../../lib/testClient';
import fs from 'fs';
import { checkReceipt } from '../../lib/receipt';

const documents = KSHDETestClient.documents;

describe('document-status resource test', () => {
    it("should get a document's status list", async () => {
        const stream = fs.createReadStream(FILE_SMALL);
        const document = createDocumentPayload(stream);
        const receipt = await documents.post(document);
        checkReceipt(receipt);
        const documentProtocolNo = receipt.DocumentProtocolNo;
        const firstPage = await documents(documentProtocolNo).status.get();
        expect(firstPage.data).toBe(undefined);
    });
});
