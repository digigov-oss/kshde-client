import { DocumentApiResult, GetDocumentsStatusOptions } from '../../../../src/v1/api/defs/document';
import NavigatableApiResult from '../../../../src/v1/api/utils/navigatable-api-result';
import { KSHDETestClient } from '../../lib/testClient';

const documents = KSHDETestClient.documents;

describe('documents resource tests', () => {
    beforeAll(async () => {
        await KSHDETestClient.authenticate();
    });

    it('should retrieve documents for all Document statuses', async () => {
        const promises: Array<Promise<NavigatableApiResult<DocumentApiResult>>> = [];
        Object.values(GetDocumentsStatusOptions).forEach(async (keyOrValue) => {
            //Both keys and values are listed
            if (typeof keyOrValue === 'string') {
                return;
            }
            promises.push(
                documents.get({
                    status: keyOrValue as GetDocumentsStatusOptions,
                    page: 1,
                    limit: 100
                })
            );
        });
        await Promise.all(promises);
    });
    it('should be able to read all documents', async () => {
        const receipts = await documents.get({
            status: GetDocumentsStatusOptions.Sent,
            limit: 100
        });
        let total = 0;
        let data = receipts.data;
        while (data) {
            total += data.length ?? 0;
            data = (await receipts.next()).data;
        }
        expect(total).toBeGreaterThan(0);
    });
});
