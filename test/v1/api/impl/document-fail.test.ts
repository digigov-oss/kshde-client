import fs from 'fs';
import { FILE_HUGE, FILE_SMALL, KSHDETestClient } from '../../lib/testClient';
import { createDocumentPayload, createUpdateDocumentPayload } from '../../lib/document';

const documents = KSHDETestClient.documents;

describe('document resource fail tests', () => {
    it.skip(
        'should fail properly when sending an oversized document',
        async () => {
            const document = createDocumentPayload(fs.createReadStream(FILE_HUGE));
            expect.assertions(5);
            try {
                await documents.post(document);
            } catch (e: any) {
                expect(e).toBeDefined();
                expect(e.SideTrackingId).toBeDefined();
                expect(e.HttpStatus).toEqual(400);
                expect(e.ErrorCode).toEqual('41');
                expect(e.ErrorMessage).toEqual(
                    'File size exceeds the maximum allowed size of 30 MB'
                );
            }
        },
        3 * 60 * 1000
    );

    it('should fail to create document with zero recipients', async () => {
        const document = createDocumentPayload(fs.createReadStream(FILE_SMALL), undefined, []);
        expect(async () => {
            await documents.post(document);
        }).rejects.toThrow('No RecipientSectorCodes given');
    });

    it('should fail to update document with zero recipients', async () => {
        const document = createUpdateDocumentPayload(fs.createReadStream(FILE_SMALL), []);
        expect(async () => {
            await documents('documentProtocolNo').put(document);
        }).rejects.toThrow('No RecipientSectorCodes given');
    });
});
