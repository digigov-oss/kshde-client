import { FILE_MEDIUM, FILE_SMALL, KSHDETestClient } from '../../lib/testClient';
import { Duplex, Readable } from 'stream';
import fs from 'fs';
import { checkReceipt } from '../../lib/receipt';
import { createDocumentPayload, updateDocument } from '../../lib/document';

const documents = KSHDETestClient.documents;

describe('document resource test', () => {
    it('should post a document from Readable', async () => {
        const documentBuffer = fs.readFileSync(FILE_SMALL);
        const uint8ArrayStream = new Duplex();

        uint8ArrayStream.push(new Uint8Array(documentBuffer));
        uint8ArrayStream.push(null);

        const readers: Readable[] = [
            fs.createReadStream(FILE_SMALL),
            uint8ArrayStream,
            Readable.from(documentBuffer)
        ];
        for (const reader of readers) {
            const document = createDocumentPayload(reader);
            const receipt = await documents.post(document);
            checkReceipt(receipt);
        }
    });

    it('should post put and get a document and it contents by version', async () => {
        const fileVersions = [FILE_SMALL, FILE_MEDIUM];
        const document = createDocumentPayload(
            fs.createReadStream(fileVersions[0]),
            fileVersions[0].split('/').pop() ?? ''
        );
        //Create
        const receipt = await documents.post(document);
        checkReceipt(receipt);
        const documentProtocolNo = receipt.DocumentProtocolNo;
        let doc = await documents(documentProtocolNo).get();
        expect(doc).toBeDefined();

        //Update
        const updateReceipt = await updateDocument(
            fs.createReadStream(fileVersions[1]),
            documentProtocolNo,
            doc
        );

        checkReceipt(updateReceipt);
        expect(updateReceipt.VersionNumber).toEqual(2);

        doc = await documents(documentProtocolNo).get({ version: 2 });
        expect(doc).toBeDefined();
        expect(doc.VersionNumber).toEqual(2);
        // Give sometime to server to hadle saving the files
        await new Promise<void>((resolve) => {
            setTimeout(() => {
                resolve();
            }, 5000);
        });
        for (const [index, fileName] of fileVersions.entries()) {
            const docVersion = index + 1;
            const docContent = await documents(documentProtocolNo).content.get({
                version: docVersion
            });
            const writeStream = fs.createWriteStream(`/tmp/downloaded_${doc.FileName}`);
            docContent.pipe(writeStream);
            await new Promise<void>((resolve) => {
                writeStream.on('close', resolve);
            });
            expect(fs.statSync(`/tmp/downloaded_${doc.FileName}`).size).toEqual(
                fs.statSync(fileName).size
            );
        }
    });

    it('Should be able to submit a big array of documents', async () => {
        const docs = new Array(40).fill('').map((value, index) => {
            return createDocumentPayload(fs.createReadStream(FILE_SMALL), `doc_${index}.pdf`);
        });
        let executed = 0;
        for (const doc of docs) {
            await documents.post(doc);
            executed++;
            const shouldLog = executed && executed % 10 === 0;
            if (shouldLog) {
                console.log(`${executed} documents have been sent`);
            }
        }
    });
});
