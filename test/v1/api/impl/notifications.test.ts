import { KSHDETestClient } from '../../lib/testClient';

const notifications = KSHDETestClient.notifications;

describe('notifications resource tests', () => {
    it('should retrieve notifications', async () => {
        const result = await notifications.get();
        expect(result).toBeDefined();
    });
});
