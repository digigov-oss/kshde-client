import { createDocumentPayload } from '../../lib/document';
import { FILE_SMALL, KSHDETestClient } from '../../lib/testClient';
import fs from 'fs';
import { checkReceipt } from '../../lib/receipt';
import { checkAttachmentResult } from '../../lib/attachment';
import { DocumentAttachmentCreateData } from '../../../../src/v1';

const documents = KSHDETestClient.documents;

describe('document-attachments test', () => {
    it('should post attachments for a document and retrieve them', async () => {
        const stream = fs.createReadStream(FILE_SMALL);
        const document = createDocumentPayload(stream, undefined, undefined, false);
        const receipt = await documents.post(document);
        checkReceipt(receipt);
        const documentProtocolNo = receipt.DocumentProtocolNo;
        const attachmentsNum = 5;
        const attachments = Array(attachmentsNum)
            .fill('')
            .map((value, index) => {
                const metadata: DocumentAttachmentCreateData = {
                    FileName: `attachment_${index}.pdf`,
                    IsFinal: index === attachmentsNum - 1
                };
                return {
                    data: fs.createReadStream(FILE_SMALL),
                    metadata: metadata
                };
            });
        for (const [index, attachment] of attachments.entries()) {
            const attachmentResult =
                await documents(documentProtocolNo).attachments.post(attachment);
            checkAttachmentResult(attachmentResult, index === attachmentsNum - 1);
        }
        const allAttachments = await documents(documentProtocolNo).attachments.get();
        expect(allAttachments).toBeDefined();
        expect(allAttachments.length).toEqual(attachmentsNum);
    });
});
