import { KSHDETestClient } from '../../lib/testClient';

const receipts = KSHDETestClient.receipts;

describe('receipt resource tests', () => {
    it('should retrieve a receipt', async () => {
        const result = await receipts.get();
        expect(result).toBeDefined();
        expect(result.data).toBeDefined();
        expect(result.data?.length).toBeGreaterThanOrEqual(1);
        if (result.data?.length) {
            const receipt = result.data[0];
            const receiptId = receipt.ReceiptId;
            const receiptCopy = await receipts(receiptId).get();
            expect(receiptCopy).toEqual(receipt);
        }
    });
});
