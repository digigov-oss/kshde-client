import { AuthenticateEndpoint } from '../../../../src/v1';
import EndpointUrl from '../../../../src/v1/api/defs/endpoint-url';
import { config, TEST_API_BASE_URL } from '../../lib/testClient';

const authenticate = new AuthenticateEndpoint(
    { baseApiURL: TEST_API_BASE_URL },
    EndpointUrl.AUTHENTICATE
);

describe('authenticate resource tests', () => {
    it('should return the access token when properly setup', async () => {
        const authApiResult = await authenticate.post(
            {},
            {
                SectorCode: config.sectorCode,
                ClientId: config.clientId,
                ClientSecret: config.clientSecret
            }
        );
        expect(authApiResult).toBeDefined();
        expect(authApiResult.AccessToken).toBeDefined();
        expect(authApiResult.TokenType).toBeDefined();
        expect(authApiResult.ExpiresOn).toBeDefined();
    });
});
