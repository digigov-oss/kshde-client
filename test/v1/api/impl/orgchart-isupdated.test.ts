import { KSHDETestClient } from '../../lib/testClient';
const orgchart = KSHDETestClient.orgchart;

describe('orgchart-isupdated resource tests', () => {
    it('should return if a version has been updated', async () => {
        const isUpdated = await orgchart(1).updated.get();
        expect(isUpdated).toBeDefined();
    });
});
