import { KSHDETestClient } from '../../lib/testClient';

const orgchart = KSHDETestClient.orgchart;
describe('orgchart resource tests', () => {
    it('should return the orgchart tree', async () => {
        const organizations = await orgchart.get();
        expect(organizations).toBeDefined();
        expect(organizations.Version).toBeGreaterThan(0);
    });
});
