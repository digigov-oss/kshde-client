import { AuthenticateEndpoint } from '../../../../src/v1';
import EndpointUrl from '../../../../src/v1/api/defs/endpoint-url';
import { TEST_API_BASE_URL } from '../../lib/testClient';

const authenticate = new AuthenticateEndpoint(
    { baseApiURL: TEST_API_BASE_URL },
    EndpointUrl.AUTHENTICATE
);

describe('authenticate resource failed tests', () => {
    it('should fail with serverApi Exception when not properly setup', async () => {
        expect.assertions(2);
        await authenticate
            .post(
                {},
                {
                    SectorCode: 'sectorCode',
                    ClientId: 'clientId',
                    ClientSecret: 'clientSecret'
                }
            )
            .catch((e) => {
                expect(e.SideTrackingId).toBeDefined();
                expect(e.HttpStatus).toEqual(401);
            });
    });
});
