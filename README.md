
# KSHDE REST Api client

## Project is currently in beta
## Description
javascript library that provides a client api the
for the central electronic document managment system (KSHDE) provided by
[mindigital](https://mindigital.gr).

Full documentation of the REST api can be found [here.](https://sdddsp.mindigital-shde.gr/docs/index.html)


## Installation

    npm install kshde-client

## Usage

### Create client
    import axiosRetry, { IAxiosRetryConfig } from 'axios-retry';

    import { KSHDEClient } from 'kshde-client/v1';
    //Optional. Change to your needs
    const retryConfig: IAxiosRetryConfig = {
        retries: 4,
        retryDelay: (...arg) => axiosRetry.exponentialDelay(...arg, 300),
        retryCondition(error: AxiosError) {
            if (error.response)
                switch (error.response.status) {
                    //retry only if status is 500 or 501
                    case 500:
                    case 501:
                        return true;
                    default:
                        return false;
                }
            return false;
        },
        onRetry: (retryCount, error) => {
            console.log(`retry count: ${retryCount} ${error.response?.config.url}`);
        },
    };

    const clientConfig = {
        sectorCode: ' a sector code ',
        clientId: ' client id ',
        clientSecret: ' client secret ',

        // Whether to run against the testing or the real endpoints
        // Optional. Defaults to 'development'
        mode: 'production' | 'development'

        // Optional. No default is supplied
        retryConfig?: retryConfig;
    }
    const client = new KSHDEClient(clientConfig)

### Use client

#### The client api is designed to match the REST api urls.

#### To issue a GET to /api/v1/documents/{protocolNo}/attachments/{attachmentId}/content

    client.documents(protocolNo).attachments(attachmentId).content.get(...)

#### To issue a POST to /api/v1/documents/{protocolNo}/attachments

    client.documents(protocolNo).attachments.post(...)

#### To issue a PUT to /api/v1/documents/{protocolNo}

    client.documents(protocolNo).put(...)

#### And so on...
## License
Copyright © 2023 [grnet](https://grnet.gr/)

KSHDE-client is licensed under the [EUPL-1.2-or-later](https://commission.europa.eu/content/european-union-public-licence_en)