"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const authenticate_1 = __importDefault(require("./api/authenticate"));
const API_BASE_URL_PRODUCTION = 'https://sddd.mindigital-shde.gr/api/v1';
const API_BASE_URL_DEVELOPMENT = 'https://sdddsp.mindigital-shde.gr/api/v1';
const utils_1 = __importDefault(require("./client/utils/utils"));
const client_1 = require("./client");
const endpoint_url_1 = __importDefault(require("./api/defs/endpoint-url"));
class KSHDEClient {
    constructor(config) {
        this.authenticate = () => __awaiter(this, void 0, void 0, function* () {
            if (!this.authData) {
                return yield this._doAuthenticate();
            }
            else {
                let refreshToken = false;
                try {
                    const authExpiresTime = Date.parse(this.authData.ExpiresOn);
                    const currTime = new Date().getTime();
                    if (currTime >= authExpiresTime) {
                        refreshToken = true;
                    }
                }
                catch (e) {
                    refreshToken = true;
                }
                if (refreshToken) {
                    return yield this._doAuthenticate();
                }
                return this.authData.AccessToken;
            }
        });
        this._doAuthenticate = () => __awaiter(this, void 0, void 0, function* () {
            const authConfig = this.authConfig;
            return yield this.authenticateEndpoint
                .post({}, {
                SectorCode: authConfig.sectorCode,
                ClientId: authConfig.clientId,
                ClientSecret: authConfig.clientSecret
            })
                .then((authData) => {
                this.authData = authData;
                return authData.AccessToken;
            })
                .catch((e) => {
                this.authData = undefined;
                throw e;
            });
        });
        const baseApiURL = config.mode === 'production' ? API_BASE_URL_PRODUCTION : API_BASE_URL_DEVELOPMENT;
        this.authConfig = {
            sectorCode: config.sectorCode,
            clientId: config.clientId,
            clientSecret: config.clientSecret
        };
        this.authenticateEndpoint = new authenticate_1.default({ baseApiURL, retryConfig: config.retryConfig }, endpoint_url_1.default.AUTHENTICATE);
        const resourceProps = {
            baseApiURL,
            authenticate: this.authenticate,
            retryConfig: config.retryConfig
        };
        this.orgchart = (0, client_1.createOrgchartClientApi)(resourceProps);
        this.documents = (0, client_1.createDocumentsClientApi)(resourceProps);
        this.notifications = (0, client_1.createNotificationsClientApi)(resourceProps);
        this.receipts = (0, client_1.createReceiptsClientApi)(resourceProps);
        this.utils = new utils_1.default(this.orgchart, this.documents);
    }
}
exports.default = KSHDEClient;
