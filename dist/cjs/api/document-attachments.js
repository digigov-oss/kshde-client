"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const navigatable_api_result_1 = __importDefault(require("./utils/navigatable-api-result"));
const save_document_1 = __importDefault(require("./utils/save-document"));
class DocumentAttachmentsEndpoint extends save_document_1.default {
    constructor() {
        super(...arguments);
        this.get = (urlParams) => __awaiter(this, void 0, void 0, function* () {
            const url = this.toActualUrl(urlParams);
            const get = (params) => __awaiter(this, void 0, void 0, function* () {
                const response = yield this._get({ url, params });
                return response.data;
            });
            const firstPage = yield new navigatable_api_result_1.default(get).init({});
            return firstPage.data ? firstPage.data : [];
        });
        this.post = (urlParams, attachment) => __awaiter(this, void 0, void 0, function* () {
            const url = this.toActualUrl(urlParams);
            return this.saveDocument(url, attachment);
        });
    }
}
exports.default = DocumentAttachmentsEndpoint;
