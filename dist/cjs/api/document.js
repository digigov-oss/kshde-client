"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const save_document_1 = __importDefault(require("./utils/save-document"));
class DocumentEndpoint extends save_document_1.default {
    constructor() {
        super(...arguments);
        this.get = (urlParams, options) => __awaiter(this, void 0, void 0, function* () {
            const url = this.toActualUrl(urlParams);
            const params = new URLSearchParams();
            if (options === null || options === void 0 ? void 0 : options.version) {
                params.append('version', options.version.toString());
            }
            const response = yield this._get({ url, params });
            return response.data;
        });
        this.put = (urlParams, document) => __awaiter(this, void 0, void 0, function* () {
            const url = this.toActualUrl(urlParams);
            if (!document.metadata.RecipientSectorCodes.length) {
                throw new Error('No RecipientSectorCodes given');
            }
            return yield this.saveDocument(url, document, true);
        });
    }
}
exports.default = DocumentEndpoint;
