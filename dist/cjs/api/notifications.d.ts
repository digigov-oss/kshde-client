import Resource from './utils/resource';
import NavigatableApiResult from './utils/navigatable-api-result';
import { GetNotificationsQueryParams, NotificationApiResult, NotificationsEndpointMethods } from './defs/notifications';
import { NoUrlParams } from '../defs/endpoint-methods';
declare class NotificationsEndpoint extends Resource implements NotificationsEndpointMethods {
    get: (urlParams: NoUrlParams, options?: GetNotificationsQueryParams) => Promise<NavigatableApiResult<NotificationApiResult>>;
}
export default NotificationsEndpoint;
