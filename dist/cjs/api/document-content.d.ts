import { Readable } from 'stream';
import Resource from './utils/resource';
import { DocumentContentEndpointMethods, GetDocumentContentQueryParams } from './defs/document-content';
import { URLParams } from '../defs/endpoint-methods';
import { DocumentURLParams } from './defs';
declare class DocumentContentEndpoint extends Resource implements DocumentContentEndpointMethods {
    get: (urlParams: URLParams<DocumentURLParams>, options?: GetDocumentContentQueryParams) => Promise<Readable>;
}
export default DocumentContentEndpoint;
