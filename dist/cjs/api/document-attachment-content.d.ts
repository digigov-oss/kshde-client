import { Readable } from 'stream';
import Resource from './utils/resource';
import { DocumentAttachmentContentEndpointMethods } from './defs/document-attachment-content';
import { URLParams } from '../defs/endpoint-methods';
import { DocumentAttachmentURLParams } from './defs';
declare class DocumentAttachmentContentEndpoint extends Resource implements DocumentAttachmentContentEndpointMethods {
    get: (urlParams: URLParams<DocumentAttachmentURLParams>) => Promise<Readable>;
}
export default DocumentAttachmentContentEndpoint;
