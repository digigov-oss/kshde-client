import { AuthApiPayload, AuthApiResult, AuthenticateEndpointMethods } from './defs/authenticate';
import { NoUrlParams } from '../defs/endpoint-methods';
import Resource from './utils/resource';
declare class AuthenticateEndpoint extends Resource implements AuthenticateEndpointMethods {
    post: (urlParams: NoUrlParams, data: AuthApiPayload) => Promise<AuthApiResult>;
}
export default AuthenticateEndpoint;
