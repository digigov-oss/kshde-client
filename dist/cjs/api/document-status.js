"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const navigatable_api_result_1 = __importDefault(require("./utils/navigatable-api-result"));
const resource_1 = __importDefault(require("./utils/resource"));
class DocumentStatusEndpoint extends resource_1.default {
    constructor() {
        super(...arguments);
        this.get = (urlParams, options) => __awaiter(this, void 0, void 0, function* () {
            var _a;
            const url = this.toActualUrl(urlParams);
            const get = (params) => __awaiter(this, void 0, void 0, function* () {
                const response = yield this._get({ url, params });
                return response.data;
            });
            const params = {
                version: (_a = options === null || options === void 0 ? void 0 : options.version) === null || _a === void 0 ? void 0 : _a.toString(),
                sectorCode: options === null || options === void 0 ? void 0 : options.sectorCode
            };
            return new navigatable_api_result_1.default(get).init({
                params,
                page: 1,
                limit: 100
            });
        });
        this.put = (urlParams, { version, data }) => __awaiter(this, void 0, void 0, function* () {
            const url = this.toActualUrl(urlParams);
            const params = new URLSearchParams({ version: version.toString() });
            const response = yield this._put({ url, params, data });
            return response.data;
        });
    }
}
exports.default = DocumentStatusEndpoint;
