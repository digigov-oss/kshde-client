"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const navigatable_api_result_1 = __importDefault(require("./utils/navigatable-api-result"));
const validateFiles_1 = require("../utils/validateFiles");
const save_document_1 = __importDefault(require("./utils/save-document"));
class DocumentsEndpoint extends save_document_1.default {
    constructor() {
        super(...arguments);
        this.get = (urlParams, options) => __awaiter(this, void 0, void 0, function* () {
            var _a, _b, _c;
            const url = this.toActualUrl(urlParams);
            const get = (params) => __awaiter(this, void 0, void 0, function* () {
                const response = yield this._get({ url, params });
                return response.data;
            });
            const params = {
                status: (_a = options === null || options === void 0 ? void 0 : options.status) === null || _a === void 0 ? void 0 : _a.toString(),
                dateFrom: (_b = options === null || options === void 0 ? void 0 : options.dateFrom) === null || _b === void 0 ? void 0 : _b.toISOString(),
                dateTo: (_c = options === null || options === void 0 ? void 0 : options.dateTo) === null || _c === void 0 ? void 0 : _c.toISOString()
            };
            return new navigatable_api_result_1.default(get).init({
                params,
                page: options === null || options === void 0 ? void 0 : options.page,
                limit: options === null || options === void 0 ? void 0 : options.limit
            });
        });
        this.post = (urlParams, document) => __awaiter(this, void 0, void 0, function* () {
            if (!document.metadata.RecipientSectorCodes.length) {
                throw new Error('No RecipientSectorCodes given');
            }
            (0, validateFiles_1.validateFileName)(document.metadata.FileName);
            const url = this.toActualUrl(urlParams);
            return yield this.saveDocument(url, document);
        });
    }
}
exports.default = DocumentsEndpoint;
