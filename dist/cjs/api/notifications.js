"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const resource_1 = __importDefault(require("./utils/resource"));
const navigatable_api_result_1 = __importDefault(require("./utils/navigatable-api-result"));
class NotificationsEndpoint extends resource_1.default {
    constructor() {
        super(...arguments);
        this.get = (urlParams, options) => __awaiter(this, void 0, void 0, function* () {
            var _a, _b, _c, _d;
            const url = this.toActualUrl(urlParams);
            const get = (params) => __awaiter(this, void 0, void 0, function* () {
                const response = yield this._get({ url, params });
                return response.data;
            });
            const params = {
                status: (_a = options === null || options === void 0 ? void 0 : options.status) === null || _a === void 0 ? void 0 : _a.toString(),
                documentProtocolNo: options === null || options === void 0 ? void 0 : options.documentProtocolNo,
                type: (_b = options === null || options === void 0 ? void 0 : options.type) === null || _b === void 0 ? void 0 : _b.toString(),
                dateFrom: (_c = options === null || options === void 0 ? void 0 : options.dateFrom) === null || _c === void 0 ? void 0 : _c.toISOString(),
                dateTo: (_d = options === null || options === void 0 ? void 0 : options.dateTo) === null || _d === void 0 ? void 0 : _d.toISOString()
            };
            return new navigatable_api_result_1.default(get).init({
                params,
                page: options === null || options === void 0 ? void 0 : options.page,
                limit: options === null || options === void 0 ? void 0 : options.limit
            });
        });
    }
}
exports.default = NotificationsEndpoint;
