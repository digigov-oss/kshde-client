import { URLParams } from '../defs/endpoint-methods';
import { DocumentAttachmentApiResult, DocumentAttachmentEndpointMethods, DocumentAttachmentURLParams } from './defs/document-attachment';
import Resource from './utils/resource';
declare class DocumentAttachmentEndpoint extends Resource implements DocumentAttachmentEndpointMethods {
    get: (urlParams: URLParams<DocumentAttachmentURLParams>) => Promise<DocumentAttachmentApiResult>;
}
export default DocumentAttachmentEndpoint;
