"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __exportStar = (this && this.__exportStar) || function(m, exports) {
    for (var p in m) if (p !== "default" && !Object.prototype.hasOwnProperty.call(exports, p)) __createBinding(exports, m, p);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReceiptsEndpoint = exports.ReceiptEndpoint = exports.OrgChartEndpoint = exports.OrgchartVersionIsUpdatedEndpoint = exports.NotificationsEndpoint = exports.DocumentsEndpoint = exports.DocumentEndpoint = exports.DocumentStatusEndpoint = exports.DocumentContentEndpoint = exports.DocumentAttachmentsEndpoint = exports.DocumentAttachmentEndpoint = exports.DocumentAttachmentContentEndpoint = exports.AuthenticateEndpoint = exports.NavigatableApiResult = void 0;
var navigatable_api_result_1 = require("./utils/navigatable-api-result");
Object.defineProperty(exports, "NavigatableApiResult", { enumerable: true, get: function () { return __importDefault(navigatable_api_result_1).default; } });
__exportStar(require("./utils/navigatable-api-result"), exports);
__exportStar(require("./defs"), exports);
var authenticate_1 = require("./authenticate");
Object.defineProperty(exports, "AuthenticateEndpoint", { enumerable: true, get: function () { return __importDefault(authenticate_1).default; } });
var document_attachment_content_1 = require("./document-attachment-content");
Object.defineProperty(exports, "DocumentAttachmentContentEndpoint", { enumerable: true, get: function () { return __importDefault(document_attachment_content_1).default; } });
var document_attachment_1 = require("./document-attachment");
Object.defineProperty(exports, "DocumentAttachmentEndpoint", { enumerable: true, get: function () { return __importDefault(document_attachment_1).default; } });
var document_attachments_1 = require("./document-attachments");
Object.defineProperty(exports, "DocumentAttachmentsEndpoint", { enumerable: true, get: function () { return __importDefault(document_attachments_1).default; } });
var document_content_1 = require("./document-content");
Object.defineProperty(exports, "DocumentContentEndpoint", { enumerable: true, get: function () { return __importDefault(document_content_1).default; } });
var document_status_1 = require("./document-status");
Object.defineProperty(exports, "DocumentStatusEndpoint", { enumerable: true, get: function () { return __importDefault(document_status_1).default; } });
var document_1 = require("./document");
Object.defineProperty(exports, "DocumentEndpoint", { enumerable: true, get: function () { return __importDefault(document_1).default; } });
var documents_1 = require("./documents");
Object.defineProperty(exports, "DocumentsEndpoint", { enumerable: true, get: function () { return __importDefault(documents_1).default; } });
var notifications_1 = require("./notifications");
Object.defineProperty(exports, "NotificationsEndpoint", { enumerable: true, get: function () { return __importDefault(notifications_1).default; } });
var orgchart_isupdated_1 = require("./orgchart-isupdated");
Object.defineProperty(exports, "OrgchartVersionIsUpdatedEndpoint", { enumerable: true, get: function () { return __importDefault(orgchart_isupdated_1).default; } });
var orgchart_1 = require("./orgchart");
Object.defineProperty(exports, "OrgChartEndpoint", { enumerable: true, get: function () { return __importDefault(orgchart_1).default; } });
var receipt_1 = require("./receipt");
Object.defineProperty(exports, "ReceiptEndpoint", { enumerable: true, get: function () { return __importDefault(receipt_1).default; } });
var receipts_1 = require("./receipts");
Object.defineProperty(exports, "ReceiptsEndpoint", { enumerable: true, get: function () { return __importDefault(receipts_1).default; } });
