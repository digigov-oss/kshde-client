"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DocumentStatus = void 0;
var DocumentStatus;
(function (DocumentStatus) {
    DocumentStatus[DocumentStatus["Received"] = 0] = "Received";
    DocumentStatus[DocumentStatus["Open"] = 1] = "Open";
    DocumentStatus[DocumentStatus["Finalized"] = 2] = "Finalized";
    DocumentStatus[DocumentStatus["Filed"] = 3] = "Filed";
    DocumentStatus[DocumentStatus["ToBeCharged"] = 4] = "ToBeCharged";
    DocumentStatus[DocumentStatus["Processed"] = 5] = "Processed";
    DocumentStatus[DocumentStatus["ToBeAnswered"] = 6] = "ToBeAnswered";
    DocumentStatus[DocumentStatus["ToBeSigned"] = 7] = "ToBeSigned";
})(DocumentStatus || (exports.DocumentStatus = DocumentStatus = {}));
