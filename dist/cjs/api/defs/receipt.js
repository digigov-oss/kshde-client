"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReceiptType = void 0;
var ReceiptType;
(function (ReceiptType) {
    ReceiptType[ReceiptType["NewDocument"] = 1] = "NewDocument";
    ReceiptType[ReceiptType["ChangeDocument"] = 2] = "ChangeDocument";
    ReceiptType[ReceiptType["ChangeDocumentStatus"] = 3] = "ChangeDocumentStatus";
    ReceiptType[ReceiptType["DocumentReceived"] = 4] = "DocumentReceived";
    ReceiptType[ReceiptType["DocumentSent"] = 5] = "DocumentSent";
})(ReceiptType || (exports.ReceiptType = ReceiptType = {}));
