import { Get } from '../../defs/endpoint-methods';
export type OrgchartNode = {
    Code: number;
    Name: string;
    NameEnglish: string | null;
    IsActive: boolean;
    IsSDDDNode: boolean;
    Departments?: OrgchartNode[];
    ChildNodes?: OrgchartNode[];
};
export type OrgchartApiResult = {
    Version: number;
    RootNode: OrgchartNode;
};
export type OrgChartPayload = {
    Departments: OrgchartNode[];
};
export type OrgChartEndpointMethods = Get<OrgchartApiResult, void, never>;
