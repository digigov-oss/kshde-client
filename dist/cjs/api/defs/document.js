"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetDocumentsStatusOptions = exports.RelatedDocumentType = exports.DocumentClasification = exports.DocumentCategory = void 0;
var DocumentCategory;
(function (DocumentCategory) {
    DocumentCategory[DocumentCategory["Decision"] = 1] = "Decision";
    DocumentCategory[DocumentCategory["Contract"] = 2] = "Contract";
    DocumentCategory[DocumentCategory["Account"] = 3] = "Account";
    DocumentCategory[DocumentCategory["Announcement"] = 4] = "Announcement";
    DocumentCategory[DocumentCategory["Other"] = 5] = "Other";
})(DocumentCategory || (exports.DocumentCategory = DocumentCategory = {}));
var DocumentClasification;
(function (DocumentClasification) {
    DocumentClasification[DocumentClasification["General"] = 0] = "General";
    DocumentClasification[DocumentClasification["Confidential"] = 1] = "Confidential";
})(DocumentClasification || (exports.DocumentClasification = DocumentClasification = {}));
var RelatedDocumentType;
(function (RelatedDocumentType) {
    RelatedDocumentType[RelatedDocumentType["None"] = 0] = "None";
    RelatedDocumentType[RelatedDocumentType["General"] = 1] = "General";
    RelatedDocumentType[RelatedDocumentType["Answer"] = 2] = "Answer";
})(RelatedDocumentType || (exports.RelatedDocumentType = RelatedDocumentType = {}));
//endpoint: documents
var GetDocumentsStatusOptions;
(function (GetDocumentsStatusOptions) {
    GetDocumentsStatusOptions[GetDocumentsStatusOptions["New"] = 0] = "New";
    GetDocumentsStatusOptions[GetDocumentsStatusOptions["Received"] = 1] = "Received";
    GetDocumentsStatusOptions[GetDocumentsStatusOptions["Sent"] = 2] = "Sent";
})(GetDocumentsStatusOptions || (exports.GetDocumentsStatusOptions = GetDocumentsStatusOptions = {}));
