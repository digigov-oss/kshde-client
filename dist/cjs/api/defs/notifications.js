"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetNotificationsStatus = exports.NotificationType = void 0;
var NotificationType;
(function (NotificationType) {
    NotificationType[NotificationType["DocumentNew"] = 1] = "DocumentNew";
    NotificationType[NotificationType["DocumentChanged"] = 2] = "DocumentChanged";
    NotificationType[NotificationType["DocumentStatusChanged"] = 3] = "DocumentStatusChanged";
    NotificationType[NotificationType["DocumentReceived"] = 4] = "DocumentReceived";
})(NotificationType || (exports.NotificationType = NotificationType = {}));
var GetNotificationsStatus;
(function (GetNotificationsStatus) {
    GetNotificationsStatus[GetNotificationsStatus["NotReceived"] = 0] = "NotReceived";
    GetNotificationsStatus[GetNotificationsStatus["Received"] = 1] = "Received";
})(GetNotificationsStatus || (exports.GetNotificationsStatus = GetNotificationsStatus = {}));
