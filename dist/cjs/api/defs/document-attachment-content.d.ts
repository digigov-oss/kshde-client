/// <reference types="node" />
import { Get } from '../../defs/endpoint-methods';
import { Readable } from 'stream';
import { DocumentAttachmentURLParams } from './document-attachment';
export type DocumentAttachmentContentEndpointMethods = Get<Readable, never, DocumentAttachmentURLParams>;
