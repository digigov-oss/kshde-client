"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var EndpointUrl;
(function (EndpointUrl) {
    EndpointUrl["AUTHENTICATE"] = "authenticate";
    EndpointUrl["DOCUMENTS"] = "documents";
    EndpointUrl["DOCUMENT"] = "documents/:documentProtocolNo";
    EndpointUrl["DOCUMENT_CONTENT"] = "documents/:documentProtocolNo/content";
    EndpointUrl["DOCUMENT_STATUS"] = "documents/:documentProtocolNo/status";
    EndpointUrl["DOCUMENT_ATTACHMENTS"] = "documents/:documentProtocolNo/attachments";
    EndpointUrl["DOCUMENT_ATTACHMENT"] = "documents/:documentProtocolNo/attachments/:attachmentId";
    EndpointUrl["DOCUMENT_ATTACHMENT_CONTENT"] = "documents/:documentProtocolNo/attachments/:attachmentId/content";
    EndpointUrl["NOTIFICATIONS"] = "notifications";
    EndpointUrl["ORCHART"] = "orgchart";
    EndpointUrl["ORGCHART_ISUPDATED"] = "orgchart/:currentVersion/isupdated";
    EndpointUrl["RECEIPTS"] = "receipts";
    EndpointUrl["RECEIPT"] = "receipts/:receiptId";
})(EndpointUrl || (EndpointUrl = {}));
exports.default = EndpointUrl;
