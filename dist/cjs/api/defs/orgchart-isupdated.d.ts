import { Get, URLParams } from '../../defs/endpoint-methods';
export type OrgchartVersionURLParams = {
    currentVersion: number;
};
export type OrgchartIsUpdatedEndpointMethods = Get<boolean, void, URLParams<OrgchartVersionURLParams>>;
