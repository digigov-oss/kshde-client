import { DocumentMetadata, DocumentPayload } from '../defs/document';
import Resource from './resource';
declare class SaveDocumentResource extends Resource {
    protected saveDocument(url: string, filePayload: DocumentPayload<DocumentMetadata>, update?: boolean): Promise<any>;
}
export default SaveDocumentResource;
