"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const MIN_PAGE = 1;
const DEFAULT_LIMIT = 50;
const MAX_LIMIT = 100;
const MIN_LIMIT = 0;
class NavigatableApiResult {
    constructor(get) {
        this.data = undefined;
        this.page = MIN_PAGE;
        this.params = new URLSearchParams();
        this.get = get;
    }
    init({ page = MIN_PAGE, limit = DEFAULT_LIMIT, params }) {
        return __awaiter(this, void 0, void 0, function* () {
            if (page < MIN_PAGE) {
                throw new Error(`Page ${page} must be greater than 0.`);
            }
            if (limit > MAX_LIMIT || limit <= MIN_LIMIT) {
                throw new Error(`Limit ${limit} must be between 0 and 100.`);
            }
            //Update state
            this.page = page;
            this.data = undefined;
            Object.keys(this.params).forEach((key) => {
                this.params.delete(key);
            });
            if (params) {
                Object.entries(params).forEach(([key, value]) => {
                    if (value) {
                        this.params.set(key, value);
                    }
                });
            }
            this.params.set('page', page.toString());
            this.params.set('limit', limit.toString());
            return this.doGet(this.page);
        });
    }
    next() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.doGet(this.page + 1);
        });
    }
    previous() {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.page === 1) {
                this.data = undefined;
                return this;
            }
            return this.doGet(this.page - 1);
        });
    }
    doGet(page) {
        return __awaiter(this, void 0, void 0, function* () {
            this.params.set('page', page.toString());
            const response = yield this.get(this.params);
            this.data = response.results.length ? response.results : undefined;
            this.page = page;
            return this;
        });
    }
}
exports.default = NavigatableApiResult;
