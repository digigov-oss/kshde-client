"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const form_data_1 = __importDefault(require("form-data"));
const resource_1 = __importDefault(require("./resource"));
class SaveDocumentResource extends resource_1.default {
    saveDocument(url, filePayload, update = false) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            const formData = new form_data_1.default();
            formData.append('DocumentContent', filePayload.data, {
                filename: (_a = filePayload.metadata.FileName) !== null && _a !== void 0 ? _a : 'document.pdf',
                contentType: 'application/octet-stream'
            });
            formData.append('DocumentMetadata', JSON.stringify(filePayload.metadata));
            if (update) {
                const response = yield this._put({ url, data: formData });
                return response.data;
            }
            else {
                const response = yield this._post({ url, data: formData });
                return response.data;
            }
        });
    }
}
exports.default = SaveDocumentResource;
