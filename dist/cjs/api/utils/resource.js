"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importStar(require("axios"));
const form_data_1 = __importDefault(require("form-data"));
const endpoint_url_1 = __importDefault(require("../defs/endpoint-url"));
const axios_retry_1 = __importDefault(require("axios-retry"));
const NonAuthenticatedUrls = [endpoint_url_1.default.AUTHENTICATE];
class Resource {
    constructor(props, endpointUrl) {
        this._get = ({ url, params, isBlob }) => __awaiter(this, void 0, void 0, function* () {
            if (!this.authenticate) {
                throw Error('No authentication method provided');
            }
            return this.authenticate().then((accessToken) => __awaiter(this, void 0, void 0, function* () {
                const responseType = isBlob ? 'stream' : undefined;
                const config = {
                    headers: {
                        Authorization: `Bearer ${accessToken}`,
                        'Cache-Control': 'no-cache'
                    },
                    params: params,
                    responseType: responseType
                };
                try {
                    return yield this.axiosClient.get(url, config);
                }
                catch (e) {
                    throw this.toError(e);
                }
            }));
        });
        this._post = ({ url, data, params }) => __awaiter(this, void 0, void 0, function* () {
            let accessToken;
            if (this.shouldAuthenticate) {
                if (!this.authenticate) {
                    throw Error('No authentication method provided');
                }
                accessToken = yield this.authenticate();
            }
            return this._save({
                url,
                data,
                params,
                accessToken
            });
        });
        this._put = ({ url, data, params }) => __awaiter(this, void 0, void 0, function* () {
            if (!this.authenticate) {
                throw Error('No authentication method provided');
            }
            const accessToken = yield this.authenticate();
            return this._save({
                url,
                data,
                params,
                update: true,
                accessToken
            });
        });
        this._save = ({ url, data, params, accessToken, update = false }) => __awaiter(this, void 0, void 0, function* () {
            const headers = data instanceof form_data_1.default
                ? Object.assign({}, data.getHeaders()) : {};
            const config = { headers: headers, params };
            if (accessToken) {
                headers['Authorization'] = `Bearer ${accessToken}`;
            }
            try {
                return update
                    ? yield this.axiosClient.put(url, data, config)
                    : yield this.axiosClient.post(url, data, config);
            }
            catch (e) {
                throw this.toError(e);
            }
        });
        this.toActualUrl = (params) => {
            return `${this.baseApiURL}/${this.replaceParams(this.endpointUrl, params)}`;
        };
        this.replaceParams = (url, params) => {
            if (!params || !Object.keys(params).length)
                return url;
            let result = url;
            Object.entries(params).forEach(([key, value]) => {
                result = result.replace(`:${key}`, value);
            });
            return result;
        };
        this.authenticate = props.authenticate;
        this.endpointUrl = endpointUrl;
        this.shouldAuthenticate = !NonAuthenticatedUrls.includes(endpointUrl);
        this.baseApiURL = props.baseApiURL;
        this.axiosClient = axios_1.default.create();
        if (props.retryConfig) {
            (0, axios_retry_1.default)(this.axiosClient, props.retryConfig);
        }
    }
    toError(error) {
        if ((0, axios_1.isAxiosError)(error)) {
            const response = error.response;
            if (response === null || response === void 0 ? void 0 : response.data) {
                return response.data;
            }
        }
        return error;
    }
}
exports.default = Resource;
