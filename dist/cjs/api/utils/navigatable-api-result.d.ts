/// <reference types="node" />
import { PagedApiResult } from '../defs/paged-result';
export type PagedApiGetFunction<T> = (params: URLSearchParams) => Promise<PagedApiResult<T>>;
export interface NavigatableApiResultInitParams {
    params?: Record<string, string | undefined>;
    page?: number;
    limit?: number;
}
declare class NavigatableApiResult<T> {
    data: Array<T> | undefined;
    private page;
    private params;
    private get;
    constructor(get: PagedApiGetFunction<T>);
    init({ page, limit, params }: NavigatableApiResultInitParams): Promise<NavigatableApiResult<T>>;
    next(): Promise<NavigatableApiResult<T>>;
    previous(): Promise<NavigatableApiResult<T>>;
    private doGet;
}
export default NavigatableApiResult;
