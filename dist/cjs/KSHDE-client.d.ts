import { Config } from './defs/config';
import Utils from './client/utils/utils';
import DocumentsClientApi from './client/defs/documents';
import NotificationsClientApi from './client/defs/notifications';
import OrgchartClientApi from './client/defs/orgchart';
import ReceiptsClientApi from './client/defs/receipts';
declare class KSHDEClient {
    private authConfig;
    private authenticateEndpoint;
    private authData?;
    documents: DocumentsClientApi;
    receipts: ReceiptsClientApi;
    orgchart: OrgchartClientApi;
    notifications: NotificationsClientApi;
    utils: Utils;
    constructor(config: Config);
    authenticate: () => Promise<string>;
    private _doAuthenticate;
}
export default KSHDEClient;
