"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const receipt_1 = __importDefault(require("../api/receipt"));
const receipts_1 = __importDefault(require("../api/receipts"));
const receipt_2 = __importDefault(require("./receipt"));
const endpoint_url_1 = __importDefault(require("../api/defs/endpoint-url"));
const createReceiptsClientApi = (resourceProps) => {
    const receipts = new receipts_1.default(resourceProps, endpoint_url_1.default.RECEIPTS);
    const receipt = new receipt_1.default(resourceProps, endpoint_url_1.default.RECEIPT);
    function f(receiptId) {
        return (0, receipt_2.default)(receiptId, receipt);
    }
    f.get = (queryParams) => {
        return receipts.get({}, queryParams);
    };
    f.post = (receipt) => {
        return receipts.post({}, receipt);
    };
    return f;
};
exports.default = createReceiptsClientApi;
