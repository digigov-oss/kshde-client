import DocumentStatusEndpoint from '../api/document-status';
import DocumentStatusClientApi from './defs/document-status';
declare const createDocumentStatusClientApi: (documentProtocolNo: string, documentStatusEndpoint: DocumentStatusEndpoint) => DocumentStatusClientApi;
export default createDocumentStatusClientApi;
