"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const document_status_1 = __importDefault(require("./document-status"));
const document_content_1 = __importDefault(require("./document-content"));
const document_attachments_1 = __importDefault(require("./document-attachments"));
const createDocumentClientApi = ({ documentProtocolNo, documentEndpoint, documentStatusEndpoint, documentContentEndpoint, documentAttachmentsEndpoint, documentAttachmentEndpoint, documentAttachmentContentEndpoint }) => {
    const urlParams = { documentProtocolNo: documentProtocolNo };
    const documentStatus = (0, document_status_1.default)(documentProtocolNo, documentStatusEndpoint);
    const documentContent = (0, document_content_1.default)(documentProtocolNo, documentContentEndpoint);
    const documentAttachments = (0, document_attachments_1.default)({
        documentProtocolNo: documentProtocolNo,
        documentAttachmentsEndpoint: documentAttachmentsEndpoint,
        documentAttachmentEndpoint: documentAttachmentEndpoint,
        documentAttachmentContentEndpoint: documentAttachmentContentEndpoint
    });
    const documentApi = {
        attachments: documentAttachments,
        content: documentContent,
        status: documentStatus,
        get: (queryParams) => {
            return documentEndpoint.get(urlParams, queryParams);
        },
        put: (payload) => {
            return documentEndpoint.put(urlParams, payload);
        }
    };
    return documentApi;
};
exports.default = createDocumentClientApi;
