"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const orgchart_isupdated_1 = __importDefault(require("./orgchart-isupdated"));
const createOrgchartVersionClientApi = (currentVersion, orgchartIsUpdated) => {
    return {
        updated: (0, orgchart_isupdated_1.default)(currentVersion, orgchartIsUpdated)
    };
};
exports.default = createOrgchartVersionClientApi;
