"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const createReceiptClientApi = (receiptId, receiptEndpoint) => {
    return {
        get: () => {
            return receiptEndpoint.get({ receiptId: receiptId });
        }
    };
};
exports.default = createReceiptClientApi;
