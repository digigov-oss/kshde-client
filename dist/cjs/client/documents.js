"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Documents = void 0;
const document_1 = __importDefault(require("../api/document"));
const documents_1 = __importDefault(require("../api/documents"));
const document_status_1 = __importDefault(require("../api/document-status"));
const document_content_1 = __importDefault(require("../api/document-content"));
const document_attachments_1 = __importDefault(require("../api/document-attachments"));
const document_attachment_1 = __importDefault(require("../api/document-attachment"));
const document_attachment_content_1 = __importDefault(require("../api/document-attachment-content"));
const document_2 = __importDefault(require("./document"));
const endpoint_url_1 = __importDefault(require("../api/defs/endpoint-url"));
const Documents = (resourceProps) => {
    const documents = new documents_1.default(resourceProps, endpoint_url_1.default.DOCUMENTS);
    const document = new document_1.default(resourceProps, endpoint_url_1.default.DOCUMENT);
    const documentStatus = new document_status_1.default(resourceProps, endpoint_url_1.default.DOCUMENT_STATUS);
    const documentContent = new document_content_1.default(resourceProps, endpoint_url_1.default.DOCUMENT_CONTENT);
    const documentAttachments = new document_attachments_1.default(resourceProps, endpoint_url_1.default.DOCUMENT_ATTACHMENTS);
    const documentAttachment = new document_attachment_1.default(resourceProps, endpoint_url_1.default.DOCUMENT_ATTACHMENT);
    const documentAttachmentContent = new document_attachment_content_1.default(resourceProps, endpoint_url_1.default.DOCUMENT_ATTACHMENT_CONTENT);
    function f(documentProtocolNo) {
        return (0, document_2.default)({
            documentProtocolNo,
            documentEndpoint: document,
            documentStatusEndpoint: documentStatus,
            documentContentEndpoint: documentContent,
            documentAttachmentsEndpoint: documentAttachments,
            documentAttachmentEndpoint: documentAttachment,
            documentAttachmentContentEndpoint: documentAttachmentContent
        });
    }
    f.get = (queryParams) => {
        return documents.get({}, queryParams);
    };
    f.post = (document) => {
        return documents.post({}, document);
    };
    return f;
};
exports.Documents = Documents;
exports.default = exports.Documents;
