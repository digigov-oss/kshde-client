"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const orgchart_isupdated_1 = __importDefault(require("../api/orgchart-isupdated"));
const orgchart_1 = __importDefault(require("../api/orgchart"));
const orgchart_version_1 = __importDefault(require("./orgchart-version"));
const endpoint_url_1 = __importDefault(require("../api/defs/endpoint-url"));
const OrgChart = (resourceProps) => {
    const orgchart = new orgchart_1.default(resourceProps, endpoint_url_1.default.ORCHART);
    const orgchartIsUpdated = new orgchart_isupdated_1.default(resourceProps, endpoint_url_1.default.ORGCHART_ISUPDATED);
    function f(currentVersion) {
        return (0, orgchart_version_1.default)(currentVersion, orgchartIsUpdated);
    }
    f.get = orgchart.get;
    f.put = orgchart.put;
    return f;
};
exports.default = OrgChart;
