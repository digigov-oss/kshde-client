"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const notifications_1 = __importDefault(require("../api/notifications"));
const endpoint_url_1 = __importDefault(require("../api/defs/endpoint-url"));
const createNotificationsClientApi = (resourceProps) => {
    const notifications = new notifications_1.default(resourceProps, endpoint_url_1.default.NOTIFICATIONS);
    return {
        get: (queryParams) => {
            return notifications.get({}, queryParams);
        }
    };
};
exports.default = createNotificationsClientApi;
