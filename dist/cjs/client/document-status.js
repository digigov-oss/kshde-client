"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const createDocumentStatusClientApi = (documentProtocolNo, documentStatusEndpoint) => {
    const urlParams = { documentProtocolNo: documentProtocolNo };
    return {
        get: (queryParams) => {
            return documentStatusEndpoint.get(urlParams, queryParams);
        },
        put: (payload) => {
            return documentStatusEndpoint.put(urlParams, payload);
        }
    };
};
exports.default = createDocumentStatusClientApi;
