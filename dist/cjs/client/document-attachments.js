"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const document_attachment_1 = __importDefault(require("./document-attachment"));
const createDocumentAttachmentsClientApi = ({ documentProtocolNo, documentAttachmentsEndpoint, documentAttachmentEndpoint, documentAttachmentContentEndpoint }) => {
    function f(attachmentId) {
        return (0, document_attachment_1.default)({
            documentProtocolNo,
            attachmentId,
            documentAttachmentEndpoint,
            documentAttachmentContentEndpoint
        });
    }
    const urlParams = { documentProtocolNo: documentProtocolNo };
    f.get = () => {
        return documentAttachmentsEndpoint.get(urlParams);
    };
    f.post = (attachment) => {
        return documentAttachmentsEndpoint.post(urlParams, attachment);
    };
    return f;
};
exports.default = createDocumentAttachmentsClientApi;
