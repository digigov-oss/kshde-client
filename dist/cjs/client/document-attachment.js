"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const document_attachment_content_1 = __importDefault(require("./document-attachment-content"));
const createDocumentAttachmentClientApi = ({ documentProtocolNo, attachmentId, documentAttachmentEndpoint, documentAttachmentContentEndpoint }) => {
    const urlParams = { documentProtocolNo: documentProtocolNo, attachmentId: attachmentId };
    const documentAttachmentContent = (0, document_attachment_content_1.default)(Object.assign(Object.assign({}, urlParams), { documentAttachmentContentEndpoint: documentAttachmentContentEndpoint }));
    return {
        content: documentAttachmentContent,
        get: () => {
            return documentAttachmentEndpoint.get(urlParams);
        }
    };
};
exports.default = createDocumentAttachmentClientApi;
