import DocumentAttachmentContentEndpoint from '../api/document-attachment-content';
import DocumentAttachmentContentClientApi from './defs/document-attachment-content';
declare const createDocumentAttachmentContentClientApi: ({ documentProtocolNo, attachmentId, documentAttachmentContentEndpoint }: {
    documentProtocolNo: string;
    attachmentId: string;
    documentAttachmentContentEndpoint: DocumentAttachmentContentEndpoint;
}) => DocumentAttachmentContentClientApi;
export default createDocumentAttachmentContentClientApi;
