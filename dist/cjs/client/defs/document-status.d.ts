import NavigatableApiResult from '../../api/utils/navigatable-api-result';
import { DocumentStatusApiResult, GetDocumentStatusQueryParams, PutDocumentStatusData, ReceiptApiResult } from '../../api/defs';
type DocumentStatusClientApi = {
    get: (queryParams?: GetDocumentStatusQueryParams) => Promise<NavigatableApiResult<DocumentStatusApiResult>>;
    put: (payload: PutDocumentStatusData) => Promise<ReceiptApiResult>;
};
export default DocumentStatusClientApi;
