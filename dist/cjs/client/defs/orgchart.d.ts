import { OrgChartPayload, OrgchartApiResult, OrgchartNode } from '../../api/defs/orgchart';
import OrgchartVersionClientApi from './orgchart-version';
type OrgchartClientApi = {
    (currentVersion: number): OrgchartVersionClientApi;
    get: () => Promise<OrgchartApiResult>;
    put: (organizationChart: OrgChartPayload) => Promise<OrgchartNode>;
};
export default OrgchartClientApi;
