import { GetNotificationsQueryParams, NotificationApiResult } from '../../api/defs/notifications';
import NavigatableApiResult from '../../api/utils/navigatable-api-result';
type NotificationsClientApi = {
    get: (queryParams?: GetNotificationsQueryParams) => Promise<NavigatableApiResult<NotificationApiResult>>;
};
export default NotificationsClientApi;
