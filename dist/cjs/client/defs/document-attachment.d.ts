import { DocumentAttachmentApiResult } from '../../api/defs';
import DocumentAttachmentContentClientApi from './document-attachment-content';
type DocumentAttachmentClientApi = {
    get: () => Promise<DocumentAttachmentApiResult>;
    content: DocumentAttachmentContentClientApi;
};
export default DocumentAttachmentClientApi;
