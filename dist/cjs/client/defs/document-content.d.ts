/// <reference types="node" />
import { GetDocumentContentQueryParams } from '../../api/defs';
import { Readable } from 'stream';
type DocumentContentClientApi = {
    get: (queryParams?: GetDocumentContentQueryParams) => Promise<Readable>;
};
export default DocumentContentClientApi;
