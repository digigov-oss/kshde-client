import DocumentAttachmentContentEndpoint from '../api/document-attachment-content';
import DocumentAttachmentEndpoint from '../api/document-attachment';
import DocumentAttachmentClientApi from './defs/document-attachment';
declare const createDocumentAttachmentClientApi: ({ documentProtocolNo, attachmentId, documentAttachmentEndpoint, documentAttachmentContentEndpoint }: {
    documentProtocolNo: string;
    attachmentId: string;
    documentAttachmentEndpoint: DocumentAttachmentEndpoint;
    documentAttachmentContentEndpoint: DocumentAttachmentContentEndpoint;
}) => DocumentAttachmentClientApi;
export default createDocumentAttachmentClientApi;
