"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const documents_utils_1 = __importDefault(require("./documents-utils"));
const orgchart_utils_1 = __importDefault(require("./orgchart-utils"));
class Utils {
    constructor(orgchart, documents) {
        this.documents = new documents_utils_1.default(documents);
        this.orgchart = new orgchart_utils_1.default(orgchart);
    }
}
exports.default = Utils;
