import { DocumentCreateData, DocumentPayload } from '../../api/defs/document';
import { DocumentAttachmentApiResult, DocumentAttachmentCreateData } from '../../api/defs/document-attachment';
import { ReceiptApiResult } from '../../api/defs/receipt';
import DocumentsClientApi from '../defs/documents';
export interface PostDocumentResult {
    receipt: ReceiptApiResult;
    attachments: DocumentAttachmentApiResult[];
}
export interface PostDocumentMetadata extends Omit<DocumentCreateData, 'IsFinal' | 'OrgChartVersion'> {
    OrgChartVersion?: number;
}
export interface PostAttachmentMetadata extends Omit<DocumentAttachmentCreateData, 'IsFinal'> {
    IsFinal?: boolean;
}
declare class DocumentsUtils {
    private documents;
    constructor(documents: DocumentsClientApi);
    postAll: (document: DocumentPayload<PostDocumentMetadata>, attachments?: DocumentPayload<PostAttachmentMetadata>[]) => Promise<PostDocumentResult>;
}
export default DocumentsUtils;
