"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const validateFiles_1 = require("../../utils/validateFiles");
class DocumentsUtils {
    constructor(documents) {
        this.postAll = (document, attachments = []) => __awaiter(this, void 0, void 0, function* () {
            var _a;
            (0, validateFiles_1.validateFiles)(document.metadata, attachments.map((attachment) => attachment.metadata));
            const documentMetadata = Object.assign(Object.assign({}, document.metadata), { IsFinal: attachments.length === 0, OrgChartVersion: (_a = document.metadata.OrgChartVersion) !== null && _a !== void 0 ? _a : 1 });
            const receipt = yield this.documents.post({
                data: document.data,
                metadata: documentMetadata
            });
            const result = { receipt, attachments: [] };
            let success = isSuccessResult(receipt);
            if ('DocumentProtocolNo' in receipt && attachments.length > 0) {
                const documentProtocolNo = receipt.DocumentProtocolNo;
                for (let i = 0; i < attachments.length; i++) {
                    const attachment = attachments[i];
                    const isFinal = success && i === attachments.length - 1 ? true : false;
                    const attachmentPayload = {
                        data: attachment.data,
                        metadata: {
                            FileName: attachment.metadata.FileName,
                            Comments: attachment.metadata.Comments,
                            IsFinal: isFinal
                        }
                    };
                    const attachmentResp = yield this.documents(documentProtocolNo).attachments.post(attachmentPayload);
                    result.attachments.push(attachmentResp);
                    success = success && isSuccessResult(attachmentResp);
                }
            }
            if (!success) {
                throw result;
            }
            return result;
        });
        this.documents = documents;
    }
}
function isSuccessResult(response) {
    return !('ErrorCode' in response);
}
exports.default = DocumentsUtils;
