import DocumentsClientApi from '../defs/documents';
import OrgchartClientApi from '../defs/orgchart';
import DocumentsUtils from './documents-utils';
import OrgchartUtils from './orgchart-utils';
declare class Utils {
    documents: DocumentsUtils;
    orgchart: OrgchartUtils;
    constructor(orgchart: OrgchartClientApi, documents: DocumentsClientApi);
}
export default Utils;
