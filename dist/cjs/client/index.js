"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __exportStar = (this && this.__exportStar) || function(m, exports) {
    for (var p in m) if (p !== "default" && !Object.prototype.hasOwnProperty.call(exports, p)) __createBinding(exports, m, p);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.createReceiptsClientApi = exports.createOrgchartClientApi = exports.createNotificationsClientApi = exports.createDocumentsClientApi = void 0;
__exportStar(require("./defs"), exports);
var documents_1 = require("./documents");
Object.defineProperty(exports, "createDocumentsClientApi", { enumerable: true, get: function () { return __importDefault(documents_1).default; } });
var notifications_1 = require("./notifications");
Object.defineProperty(exports, "createNotificationsClientApi", { enumerable: true, get: function () { return __importDefault(notifications_1).default; } });
var orgchart_1 = require("./orgchart");
Object.defineProperty(exports, "createOrgchartClientApi", { enumerable: true, get: function () { return __importDefault(orgchart_1).default; } });
var receipts_1 = require("./receipts");
Object.defineProperty(exports, "createReceiptsClientApi", { enumerable: true, get: function () { return __importDefault(receipts_1).default; } });
__exportStar(require("./utils"), exports);
