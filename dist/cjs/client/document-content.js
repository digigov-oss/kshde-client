"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const createDocumentContentClientApi = (documentProtocolNo, documentContentEndpoint) => {
    const urlParams = { documentProtocolNo: documentProtocolNo };
    return {
        get: (queryParams) => {
            return documentContentEndpoint.get(urlParams, queryParams);
        }
    };
};
exports.default = createDocumentContentClientApi;
