"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const createDocumentAttachmentContentClientApi = ({ documentProtocolNo, attachmentId, documentAttachmentContentEndpoint }) => {
    const urlParams = { documentProtocolNo: documentProtocolNo, attachmentId: attachmentId };
    return {
        get: () => {
            return documentAttachmentContentEndpoint.get(urlParams);
        }
    };
};
exports.default = createDocumentAttachmentContentClientApi;
