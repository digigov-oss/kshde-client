import OrgchartIsUpdatedEndpoint from '../api/orgchart-isupdated';
import OrgchartIsUpdatedClientApi from './defs/orgchart-isupdated';
declare const createOrgchartIsUpdatedClientApi: (currentVersion: number, orgchartIsUpdatedEndpoint: OrgchartIsUpdatedEndpoint) => OrgchartIsUpdatedClientApi;
export default createOrgchartIsUpdatedClientApi;
