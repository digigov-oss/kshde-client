"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.isFilenameValid = exports.validateFiles = exports.validateFileName = void 0;
function validateFileName(fileName) {
    if (!isFilenameValid(fileName)) {
        throw new Error(`FileName ${fileName} contains special characters or starts or ends with dot or is longer than 255 characters`);
    }
}
exports.validateFileName = validateFileName;
function validateFiles(document, attachments) {
    const fileNames = [];
    function validateFile(file) {
        if (file.FileName) {
            if (fileNames.includes(file.FileName))
                throw new Error(`FileName ${file.FileName} is not unique`);
            validateFileName(file.FileName);
            fileNames.push(file.FileName);
        }
    }
    validateFile(document);
    attachments === null || attachments === void 0 ? void 0 : attachments.forEach((attachment) => validateFile(attachment));
}
exports.validateFiles = validateFiles;
function isFilenameValid(filename) {
    //No dot at begining or end.
    //No ;/:*%<>|" anywhere
    return !/^[.]|[;/:*%<>|"]|[.]$/.test(filename) && filename.length <= 255;
}
exports.isFilenameValid = isFilenameValid;
