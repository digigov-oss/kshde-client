import createDocumentAttachmentContentClientApi from './document-attachment-content';
const createDocumentAttachmentClientApi = ({ documentProtocolNo, attachmentId, documentAttachmentEndpoint, documentAttachmentContentEndpoint }) => {
    const urlParams = { documentProtocolNo: documentProtocolNo, attachmentId: attachmentId };
    const documentAttachmentContent = createDocumentAttachmentContentClientApi({
        ...urlParams,
        documentAttachmentContentEndpoint: documentAttachmentContentEndpoint
    });
    return {
        content: documentAttachmentContent,
        get: () => {
            return documentAttachmentEndpoint.get(urlParams);
        }
    };
};
export default createDocumentAttachmentClientApi;
