import DocumentAttachmentContentEndpoint from '../api/document-attachment-content';
import DocumentAttachmentEndpoint from '../api/document-attachment';
import DocumentAttachmentsEndpoint from '../api/document-attachments';
import DocumentAttachmentsClientApi from './defs/document-attachments';
declare const createDocumentAttachmentsClientApi: ({ documentProtocolNo, documentAttachmentsEndpoint, documentAttachmentEndpoint, documentAttachmentContentEndpoint }: {
    documentProtocolNo: string;
    documentAttachmentsEndpoint: DocumentAttachmentsEndpoint;
    documentAttachmentEndpoint: DocumentAttachmentEndpoint;
    documentAttachmentContentEndpoint: DocumentAttachmentContentEndpoint;
}) => DocumentAttachmentsClientApi;
export default createDocumentAttachmentsClientApi;
