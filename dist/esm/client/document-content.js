const createDocumentContentClientApi = (documentProtocolNo, documentContentEndpoint) => {
    const urlParams = { documentProtocolNo: documentProtocolNo };
    return {
        get: (queryParams) => {
            return documentContentEndpoint.get(urlParams, queryParams);
        }
    };
};
export default createDocumentContentClientApi;
