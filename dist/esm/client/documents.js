import DocumentEndpoint from '../api/document';
import DocumentsEndpoint from '../api/documents';
import DocumentStatusEndpoint from '../api/document-status';
import DocumentContentEndpoint from '../api/document-content';
import DocumentAttachmentsEndpoint from '../api/document-attachments';
import DocumentAttachmentEndpoint from '../api/document-attachment';
import DocumentAttachmentContentEndpoint from '../api/document-attachment-content';
import createDocumentClientApi from './document';
import EndpointUrl from '../api/defs/endpoint-url';
export const Documents = (resourceProps) => {
    const documents = new DocumentsEndpoint(resourceProps, EndpointUrl.DOCUMENTS);
    const document = new DocumentEndpoint(resourceProps, EndpointUrl.DOCUMENT);
    const documentStatus = new DocumentStatusEndpoint(resourceProps, EndpointUrl.DOCUMENT_STATUS);
    const documentContent = new DocumentContentEndpoint(resourceProps, EndpointUrl.DOCUMENT_CONTENT);
    const documentAttachments = new DocumentAttachmentsEndpoint(resourceProps, EndpointUrl.DOCUMENT_ATTACHMENTS);
    const documentAttachment = new DocumentAttachmentEndpoint(resourceProps, EndpointUrl.DOCUMENT_ATTACHMENT);
    const documentAttachmentContent = new DocumentAttachmentContentEndpoint(resourceProps, EndpointUrl.DOCUMENT_ATTACHMENT_CONTENT);
    function f(documentProtocolNo) {
        return createDocumentClientApi({
            documentProtocolNo,
            documentEndpoint: document,
            documentStatusEndpoint: documentStatus,
            documentContentEndpoint: documentContent,
            documentAttachmentsEndpoint: documentAttachments,
            documentAttachmentEndpoint: documentAttachment,
            documentAttachmentContentEndpoint: documentAttachmentContent
        });
    }
    f.get = (queryParams) => {
        return documents.get({}, queryParams);
    };
    f.post = (document) => {
        return documents.post({}, document);
    };
    return f;
};
export default Documents;
