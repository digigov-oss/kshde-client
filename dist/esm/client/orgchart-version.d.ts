import OrgchartIsUpdatedEndpoint from '../api/orgchart-isupdated';
import OrgchartVersionClientApi from './defs/orgchart-version';
declare const createOrgchartVersionClientApi: (currentVersion: number, orgchartIsUpdated: OrgchartIsUpdatedEndpoint) => OrgchartVersionClientApi;
export default createOrgchartVersionClientApi;
