import DocumentEndpoint from '../api/document';
import DocumentClientApi from './defs/document';
import DocumentStatusEndpoint from 'src/v1/api/document-status';
import DocumentContentEndpoint from 'src/v1/api/document-content';
import DocumentAttachmentsEndpoint from 'src/v1/api/document-attachments';
import DocumentAttachmentEndpoint from 'src/v1/api/document-attachment';
import DocumentAttachmentContentEndpoint from 'src/v1/api/document-attachment-content';
interface Params {
    documentProtocolNo: string;
    documentEndpoint: DocumentEndpoint;
    documentStatusEndpoint: DocumentStatusEndpoint;
    documentContentEndpoint: DocumentContentEndpoint;
    documentAttachmentsEndpoint: DocumentAttachmentsEndpoint;
    documentAttachmentEndpoint: DocumentAttachmentEndpoint;
    documentAttachmentContentEndpoint: DocumentAttachmentContentEndpoint;
}
declare const createDocumentClientApi: ({ documentProtocolNo, documentEndpoint, documentStatusEndpoint, documentContentEndpoint, documentAttachmentsEndpoint, documentAttachmentEndpoint, documentAttachmentContentEndpoint }: Params) => DocumentClientApi;
export default createDocumentClientApi;
