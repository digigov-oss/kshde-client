const createOrgchartIsUpdatedClientApi = (currentVersion, orgchartIsUpdatedEndpoint) => {
    return {
        get: async () => {
            return orgchartIsUpdatedEndpoint.get({ currentVersion: currentVersion.toString() });
        }
    };
};
export default createOrgchartIsUpdatedClientApi;
