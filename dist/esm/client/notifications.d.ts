import NotificationsClientApi from './defs/notifications';
import { ResourceProps } from '../api/utils/resource';
declare const createNotificationsClientApi: (resourceProps: ResourceProps) => NotificationsClientApi;
export default createNotificationsClientApi;
