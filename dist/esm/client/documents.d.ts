import DocumentsClientApi from './defs/documents';
import { ResourceProps } from '../api/utils/resource';
export declare const Documents: (resourceProps: ResourceProps) => DocumentsClientApi;
export default Documents;
