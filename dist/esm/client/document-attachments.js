import createDocumentAttachmentClientApi from './document-attachment';
const createDocumentAttachmentsClientApi = ({ documentProtocolNo, documentAttachmentsEndpoint, documentAttachmentEndpoint, documentAttachmentContentEndpoint }) => {
    function f(attachmentId) {
        return createDocumentAttachmentClientApi({
            documentProtocolNo,
            attachmentId,
            documentAttachmentEndpoint,
            documentAttachmentContentEndpoint
        });
    }
    const urlParams = { documentProtocolNo: documentProtocolNo };
    f.get = () => {
        return documentAttachmentsEndpoint.get(urlParams);
    };
    f.post = (attachment) => {
        return documentAttachmentsEndpoint.post(urlParams, attachment);
    };
    return f;
};
export default createDocumentAttachmentsClientApi;
