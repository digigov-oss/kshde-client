import ReceiptsClientApi from './defs/receipts';
import { ResourceProps } from '../api/utils/resource';
declare const createReceiptsClientApi: (resourceProps: ResourceProps) => ReceiptsClientApi;
export default createReceiptsClientApi;
