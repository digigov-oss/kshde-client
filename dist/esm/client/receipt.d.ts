import ReceiptEndpoint from '../api/receipt';
import ReceiptClientApi from './defs/receipt';
declare const createReceiptClientApi: (receiptId: string, receiptEndpoint: ReceiptEndpoint) => ReceiptClientApi;
export default createReceiptClientApi;
