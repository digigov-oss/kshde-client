import OrgchartIsUpdatedEndpoint from '../api/orgchart-isupdated';
import OrgChartEndpoint from '../api/orgchart';
import createOrgchartVersionClientApi from './orgchart-version';
import EndpointUrl from '../api/defs/endpoint-url';
const OrgChart = (resourceProps) => {
    const orgchart = new OrgChartEndpoint(resourceProps, EndpointUrl.ORCHART);
    const orgchartIsUpdated = new OrgchartIsUpdatedEndpoint(resourceProps, EndpointUrl.ORGCHART_ISUPDATED);
    function f(currentVersion) {
        return createOrgchartVersionClientApi(currentVersion, orgchartIsUpdated);
    }
    f.get = orgchart.get;
    f.put = orgchart.put;
    return f;
};
export default OrgChart;
