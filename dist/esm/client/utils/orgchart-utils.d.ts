import { OrgchartNode } from '../../api/defs/orgchart';
import OrgchartClientApi from '../defs/orgchart';
export interface FlatOrgchartNode extends OrgchartNode {
    ParentCode?: number;
}
declare class OrgchartUtils {
    private orgchart;
    constructor(orgchart: OrgchartClientApi);
    toArray: () => Promise<FlatOrgchartNode[]>;
}
export default OrgchartUtils;
