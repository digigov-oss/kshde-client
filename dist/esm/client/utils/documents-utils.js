import { validateFiles } from '../../utils/validateFiles';
class DocumentsUtils {
    documents;
    constructor(documents) {
        this.documents = documents;
    }
    postAll = async (document, attachments = []) => {
        validateFiles(document.metadata, attachments.map((attachment) => attachment.metadata));
        const documentMetadata = {
            ...document.metadata,
            IsFinal: attachments.length === 0,
            OrgChartVersion: document.metadata.OrgChartVersion ?? 1
        };
        const receipt = await this.documents.post({
            data: document.data,
            metadata: documentMetadata
        });
        const result = { receipt, attachments: [] };
        let success = isSuccessResult(receipt);
        if ('DocumentProtocolNo' in receipt && attachments.length > 0) {
            const documentProtocolNo = receipt.DocumentProtocolNo;
            for (let i = 0; i < attachments.length; i++) {
                const attachment = attachments[i];
                const isFinal = success && i === attachments.length - 1 ? true : false;
                const attachmentPayload = {
                    data: attachment.data,
                    metadata: {
                        FileName: attachment.metadata.FileName,
                        Comments: attachment.metadata.Comments,
                        IsFinal: isFinal
                    }
                };
                const attachmentResp = await this.documents(documentProtocolNo).attachments.post(attachmentPayload);
                result.attachments.push(attachmentResp);
                success = success && isSuccessResult(attachmentResp);
            }
        }
        if (!success) {
            throw result;
        }
        return result;
    };
}
function isSuccessResult(response) {
    return !('ErrorCode' in response);
}
export default DocumentsUtils;
