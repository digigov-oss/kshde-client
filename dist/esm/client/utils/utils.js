import DocumentsUtils from './documents-utils';
import OrgchartUtils from './orgchart-utils';
class Utils {
    documents;
    orgchart;
    constructor(orgchart, documents) {
        this.documents = new DocumentsUtils(documents);
        this.orgchart = new OrgchartUtils(orgchart);
    }
}
export default Utils;
