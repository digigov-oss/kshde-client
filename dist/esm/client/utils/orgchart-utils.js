class OrgchartUtils {
    orgchart;
    constructor(orgchart) {
        this.orgchart = orgchart;
    }
    toArray = async () => {
        const response = await this.orgchart.get();
        return flattenOrgChart(response.RootNode.ChildNodes);
    };
}
function flattenOrgChart(childNodes) {
    if (!childNodes) {
        return [];
    }
    return childNodes.reduce((acc, node) => {
        const { ChildNodes, ...rest } = node;
        return [
            ...acc,
            rest,
            ...flattenOrgChart(ChildNodes).map((n) => ({
                ...n,
                ParentCode: rest.Code
            }))
        ];
    }, []);
}
export default OrgchartUtils;
