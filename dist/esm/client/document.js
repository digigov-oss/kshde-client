import createDocumentStatusClientApi from './document-status';
import createDocumentContentClientApi from './document-content';
import createDocumentAttachmentsClientApi from './document-attachments';
const createDocumentClientApi = ({ documentProtocolNo, documentEndpoint, documentStatusEndpoint, documentContentEndpoint, documentAttachmentsEndpoint, documentAttachmentEndpoint, documentAttachmentContentEndpoint }) => {
    const urlParams = { documentProtocolNo: documentProtocolNo };
    const documentStatus = createDocumentStatusClientApi(documentProtocolNo, documentStatusEndpoint);
    const documentContent = createDocumentContentClientApi(documentProtocolNo, documentContentEndpoint);
    const documentAttachments = createDocumentAttachmentsClientApi({
        documentProtocolNo: documentProtocolNo,
        documentAttachmentsEndpoint: documentAttachmentsEndpoint,
        documentAttachmentEndpoint: documentAttachmentEndpoint,
        documentAttachmentContentEndpoint: documentAttachmentContentEndpoint
    });
    const documentApi = {
        attachments: documentAttachments,
        content: documentContent,
        status: documentStatus,
        get: (queryParams) => {
            return documentEndpoint.get(urlParams, queryParams);
        },
        put: (payload) => {
            return documentEndpoint.put(urlParams, payload);
        }
    };
    return documentApi;
};
export default createDocumentClientApi;
