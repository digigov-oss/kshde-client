const createReceiptClientApi = (receiptId, receiptEndpoint) => {
    return {
        get: () => {
            return receiptEndpoint.get({ receiptId: receiptId });
        }
    };
};
export default createReceiptClientApi;
