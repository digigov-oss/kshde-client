import NotificationsEndpoint from '../api/notifications';
import EndpointUrl from '../api/defs/endpoint-url';
const createNotificationsClientApi = (resourceProps) => {
    const notifications = new NotificationsEndpoint(resourceProps, EndpointUrl.NOTIFICATIONS);
    return {
        get: (queryParams) => {
            return notifications.get({}, queryParams);
        }
    };
};
export default createNotificationsClientApi;
