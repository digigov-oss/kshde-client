const createDocumentStatusClientApi = (documentProtocolNo, documentStatusEndpoint) => {
    const urlParams = { documentProtocolNo: documentProtocolNo };
    return {
        get: (queryParams) => {
            return documentStatusEndpoint.get(urlParams, queryParams);
        },
        put: (payload) => {
            return documentStatusEndpoint.put(urlParams, payload);
        }
    };
};
export default createDocumentStatusClientApi;
