import createOrgchartIsUpdatedClientApi from './orgchart-isupdated';
const createOrgchartVersionClientApi = (currentVersion, orgchartIsUpdated) => {
    return {
        updated: createOrgchartIsUpdatedClientApi(currentVersion, orgchartIsUpdated)
    };
};
export default createOrgchartVersionClientApi;
