import OrgchartClientApi from './defs/orgchart';
import { ResourceProps } from '../api/utils/resource';
declare const OrgChart: (resourceProps: ResourceProps) => OrgchartClientApi;
export default OrgChart;
