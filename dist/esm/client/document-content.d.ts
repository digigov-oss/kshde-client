import DocumentContentEndpoint from '../api/document-content';
import DocumentContentClientApi from './defs/document-content';
declare const createDocumentContentClientApi: (documentProtocolNo: string, documentContentEndpoint: DocumentContentEndpoint) => DocumentContentClientApi;
export default createDocumentContentClientApi;
