const createDocumentAttachmentContentClientApi = ({ documentProtocolNo, attachmentId, documentAttachmentContentEndpoint }) => {
    const urlParams = { documentProtocolNo: documentProtocolNo, attachmentId: attachmentId };
    return {
        get: () => {
            return documentAttachmentContentEndpoint.get(urlParams);
        }
    };
};
export default createDocumentAttachmentContentClientApi;
