import { ReceiptApiResult } from '../../api/defs/receipt';
type ReceiptClientApi = {
    get: () => Promise<ReceiptApiResult>;
};
export default ReceiptClientApi;
