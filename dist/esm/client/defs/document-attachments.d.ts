import { DocumentAttachmentApiResult, DocumentAttachmentCreateData, DocumentPayload } from '../../api/defs';
import DocumentAttachmentClientApi from './document-attachment';
type DocumentAttachmentsClientApi = {
    (attachmentId: string): DocumentAttachmentClientApi;
    get: () => Promise<DocumentAttachmentApiResult[]>;
    post: (attachment: DocumentPayload<DocumentAttachmentCreateData>) => Promise<DocumentAttachmentApiResult>;
};
export default DocumentAttachmentsClientApi;
