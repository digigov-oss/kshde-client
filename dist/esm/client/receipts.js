import ReceiptEndpoint from '../api/receipt';
import ReceiptsEndpoint from '../api/receipts';
import createReceiptClientApi from './receipt';
import EndpointUrl from '../api/defs/endpoint-url';
const createReceiptsClientApi = (resourceProps) => {
    const receipts = new ReceiptsEndpoint(resourceProps, EndpointUrl.RECEIPTS);
    const receipt = new ReceiptEndpoint(resourceProps, EndpointUrl.RECEIPT);
    function f(receiptId) {
        return createReceiptClientApi(receiptId, receipt);
    }
    f.get = (queryParams) => {
        return receipts.get({}, queryParams);
    };
    f.post = (receipt) => {
        return receipts.post({}, receipt);
    };
    return f;
};
export default createReceiptsClientApi;
