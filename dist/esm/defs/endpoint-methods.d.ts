export type URLParamsType = Record<string, string | number | never>;
export type URLParams<T extends URLParamsType> = {
    [key in keyof T]: string;
};
export type NoUrlParams = URLParams<Record<string, never>>;
export interface GetParams<T, U extends Record<string, string | number>> {
    queryParams: T;
    urlParams: URLParams<U>;
}
export interface Get<TResult, TParams, TURLParams extends Record<string, string | number>> {
    get: (urlParams: URLParams<TURLParams>, queryParams?: TParams) => Promise<TResult>;
}
export interface Post<TResult, TPayload, TURLParams extends Record<string, string | number>> {
    post: (urParams: URLParams<TURLParams>, payload: TPayload) => Promise<TResult>;
}
export interface Put<TResult, TPayload, TURLParams extends Record<string, string | number>> {
    put: (urParams: URLParams<TURLParams>, payload: TPayload) => Promise<TResult>;
}
