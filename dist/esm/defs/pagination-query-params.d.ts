export interface PaginationQueryParams {
    page?: number;
    limit?: number;
}
