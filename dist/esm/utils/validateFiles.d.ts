interface NamedFile {
    FileName: string;
}
export declare function validateFileName(fileName: string): void;
export declare function validateFiles(document: NamedFile, attachments?: NamedFile[]): void;
export declare function isFilenameValid(filename: string): boolean;
export {};
