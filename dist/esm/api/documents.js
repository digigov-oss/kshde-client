import NavigatableApiResult from './utils/navigatable-api-result';
import { validateFileName } from '../utils/validateFiles';
import SaveDocumentResource from './utils/save-document';
class DocumentsEndpoint extends SaveDocumentResource {
    get = async (urlParams, options) => {
        const url = this.toActualUrl(urlParams);
        const get = async (params) => {
            const response = await this._get({ url, params });
            return response.data;
        };
        const params = {
            status: options?.status?.toString(),
            dateFrom: options?.dateFrom?.toISOString(),
            dateTo: options?.dateTo?.toISOString()
        };
        return new NavigatableApiResult(get).init({
            params,
            page: options?.page,
            limit: options?.limit
        });
    };
    post = async (urlParams, document) => {
        if (!document.metadata.RecipientSectorCodes.length) {
            throw new Error('No RecipientSectorCodes given');
        }
        validateFileName(document.metadata.FileName);
        const url = this.toActualUrl(urlParams);
        return await this.saveDocument(url, document);
    };
}
export default DocumentsEndpoint;
