import Resource from './utils/resource';
import NavigatableApiResult from './utils/navigatable-api-result';
class ReceiptsEndpoint extends Resource {
    get = async (urlParams, options) => {
        const url = this.toActualUrl(urlParams);
        const get = async (params) => {
            const response = await this._get({ url, params });
            return response.data;
        };
        const params = {
            documentProtocolNo: options?.documentProtocolNo,
            type: options?.type?.toString(),
            dateFrom: options?.dateFrom?.toISOString(),
            dateTo: options?.dateTo?.toISOString()
        };
        return new NavigatableApiResult(get).init({
            params,
            page: options?.page,
            limit: options?.limit
        });
    };
    post = async (urlParams, data) => {
        const url = this.toActualUrl(urlParams);
        const response = await this._post({ url, data });
        return response.data;
    };
}
export default ReceiptsEndpoint;
