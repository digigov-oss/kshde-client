import Resource from './utils/resource';
class OrgChartEndpoint extends Resource {
    get = async () => {
        const url = this.toActualUrl({});
        const response = await this._get({ url });
        return response.data;
    };
    put = async (organizationChart) => {
        const url = this.toActualUrl({});
        const response = await this._put({ url, data: organizationChart });
        return response.data;
    };
}
export default OrgChartEndpoint;
