import Resource from './utils/resource';
class OrgchartVersionIsUpdatedEndpoint extends Resource {
    get = async (urlParams) => {
        const url = this.toActualUrl(urlParams);
        const response = await this._get({ url });
        return response.data;
    };
}
export default OrgchartVersionIsUpdatedEndpoint;
