/// <reference types="node" />
import { URLParams, URLParamsType } from '../../defs';
import EndpointUrl from '../defs/endpoint-url';
import { IAxiosRetryConfig } from 'axios-retry';
interface PostParams {
    url: string;
    data?: any;
    params?: URLSearchParams;
}
type PutParams = PostParams;
interface GetParams {
    url: string;
    params?: URLSearchParams;
    isBlob?: true;
}
export interface ResourceProps {
    baseApiURL: string;
    authenticate?: () => Promise<string>;
    retryConfig?: IAxiosRetryConfig;
}
declare class Resource {
    private baseApiURL;
    private authenticate?;
    private endpointUrl;
    private shouldAuthenticate;
    private axiosClient;
    constructor(props: ResourceProps, endpointUrl: EndpointUrl);
    protected _get: ({ url, params, isBlob }: GetParams) => Promise<import("axios").AxiosResponse<any, any>>;
    protected _post: ({ url, data, params }: PostParams) => Promise<import("axios").AxiosResponse<any, any>>;
    protected _put: ({ url, data, params }: PutParams) => Promise<import("axios").AxiosResponse<any, any>>;
    private _save;
    protected toError(error: any): any;
    protected toActualUrl: <T extends URLParamsType>(params?: URLParams<T> | undefined) => string;
    private replaceParams;
}
export default Resource;
