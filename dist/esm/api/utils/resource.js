import axios, { isAxiosError } from 'axios';
import FormData from 'form-data';
import EndpointUrl from '../defs/endpoint-url';
import axiosRetry from 'axios-retry';
const NonAuthenticatedUrls = [EndpointUrl.AUTHENTICATE];
class Resource {
    baseApiURL;
    authenticate;
    endpointUrl;
    shouldAuthenticate;
    axiosClient;
    constructor(props, endpointUrl) {
        this.authenticate = props.authenticate;
        this.endpointUrl = endpointUrl;
        this.shouldAuthenticate = !NonAuthenticatedUrls.includes(endpointUrl);
        this.baseApiURL = props.baseApiURL;
        this.axiosClient = axios.create();
        if (props.retryConfig) {
            axiosRetry(this.axiosClient, props.retryConfig);
        }
    }
    _get = async ({ url, params, isBlob }) => {
        if (!this.authenticate) {
            throw Error('No authentication method provided');
        }
        return this.authenticate().then(async (accessToken) => {
            const responseType = isBlob ? 'stream' : undefined;
            const config = {
                headers: {
                    Authorization: `Bearer ${accessToken}`,
                    'Cache-Control': 'no-cache'
                },
                params: params,
                responseType: responseType
            };
            try {
                return await this.axiosClient.get(url, config);
            }
            catch (e) {
                throw this.toError(e);
            }
        });
    };
    _post = async ({ url, data, params }) => {
        let accessToken;
        if (this.shouldAuthenticate) {
            if (!this.authenticate) {
                throw Error('No authentication method provided');
            }
            accessToken = await this.authenticate();
        }
        return this._save({
            url,
            data,
            params,
            accessToken
        });
    };
    _put = async ({ url, data, params }) => {
        if (!this.authenticate) {
            throw Error('No authentication method provided');
        }
        const accessToken = await this.authenticate();
        return this._save({
            url,
            data,
            params,
            update: true,
            accessToken
        });
    };
    _save = async ({ url, data, params, accessToken, update = false }) => {
        const headers = data instanceof FormData
            ? {
                ...data.getHeaders()
            }
            : {};
        const config = { headers: headers, params };
        if (accessToken) {
            headers['Authorization'] = `Bearer ${accessToken}`;
        }
        try {
            return update
                ? await this.axiosClient.put(url, data, config)
                : await this.axiosClient.post(url, data, config);
        }
        catch (e) {
            throw this.toError(e);
        }
    };
    toError(error) {
        if (isAxiosError(error)) {
            const response = error.response;
            if (response?.data) {
                return response.data;
            }
        }
        return error;
    }
    toActualUrl = (params) => {
        return `${this.baseApiURL}/${this.replaceParams(this.endpointUrl, params)}`;
    };
    replaceParams = (url, params) => {
        if (!params || !Object.keys(params).length)
            return url;
        let result = url;
        Object.entries(params).forEach(([key, value]) => {
            result = result.replace(`:${key}`, value);
        });
        return result;
    };
}
export default Resource;
