const MIN_PAGE = 1;
const DEFAULT_LIMIT = 50;
const MAX_LIMIT = 100;
const MIN_LIMIT = 0;
class NavigatableApiResult {
    data = undefined;
    page = MIN_PAGE;
    params = new URLSearchParams();
    get;
    constructor(get) {
        this.get = get;
    }
    async init({ page = MIN_PAGE, limit = DEFAULT_LIMIT, params }) {
        if (page < MIN_PAGE) {
            throw new Error(`Page ${page} must be greater than 0.`);
        }
        if (limit > MAX_LIMIT || limit <= MIN_LIMIT) {
            throw new Error(`Limit ${limit} must be between 0 and 100.`);
        }
        //Update state
        this.page = page;
        this.data = undefined;
        Object.keys(this.params).forEach((key) => {
            this.params.delete(key);
        });
        if (params) {
            Object.entries(params).forEach(([key, value]) => {
                if (value) {
                    this.params.set(key, value);
                }
            });
        }
        this.params.set('page', page.toString());
        this.params.set('limit', limit.toString());
        return this.doGet(this.page);
    }
    async next() {
        return await this.doGet(this.page + 1);
    }
    async previous() {
        if (this.page === 1) {
            this.data = undefined;
            return this;
        }
        return this.doGet(this.page - 1);
    }
    async doGet(page) {
        this.params.set('page', page.toString());
        const response = await this.get(this.params);
        this.data = response.results.length ? response.results : undefined;
        this.page = page;
        return this;
    }
}
export default NavigatableApiResult;
