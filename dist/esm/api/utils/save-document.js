import FormData from 'form-data';
import Resource from './resource';
class SaveDocumentResource extends Resource {
    async saveDocument(url, filePayload, update = false) {
        const formData = new FormData();
        formData.append('DocumentContent', filePayload.data, {
            filename: filePayload.metadata.FileName ?? 'document.pdf',
            contentType: 'application/octet-stream'
        });
        formData.append('DocumentMetadata', JSON.stringify(filePayload.metadata));
        if (update) {
            const response = await this._put({ url, data: formData });
            return response.data;
        }
        else {
            const response = await this._post({ url, data: formData });
            return response.data;
        }
    }
}
export default SaveDocumentResource;
