import { DocumentAttachmentApiResult, DocumentAttachmentCreateData } from './defs/document-attachment';
import { DocumentAttachmentsEndpointMethods } from './defs/document-attachment';
import { URLParams } from '../defs/endpoint-methods';
import { DocumentPayload, DocumentURLParams } from './defs';
import SaveDocumentResource from './utils/save-document';
declare class DocumentAttachmentsEndpoint extends SaveDocumentResource implements DocumentAttachmentsEndpointMethods {
    get: (urlParams: URLParams<DocumentURLParams>) => Promise<DocumentAttachmentApiResult[]>;
    post: (urlParams: URLParams<DocumentURLParams>, attachment: DocumentPayload<DocumentAttachmentCreateData>) => Promise<DocumentAttachmentApiResult>;
}
export default DocumentAttachmentsEndpoint;
