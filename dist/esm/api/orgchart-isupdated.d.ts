import Resource from './utils/resource';
import { URLParams } from '../defs/endpoint-methods';
import { OrgchartIsUpdatedEndpointMethods, OrgchartVersionURLParams } from './defs/orgchart-isupdated';
declare class OrgchartVersionIsUpdatedEndpoint extends Resource implements OrgchartIsUpdatedEndpointMethods {
    get: (urlParams: URLParams<OrgchartVersionURLParams>) => Promise<boolean>;
}
export default OrgchartVersionIsUpdatedEndpoint;
