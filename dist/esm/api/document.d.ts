import SaveDocumentResource from './utils/save-document';
import { DocumentApiResult, DocumentEndpointMethods, DocumentPayload, DocumentURLParams, DocumentUpdateData, GetDocumentQueryParams } from './defs/document';
import { ReceiptApiResult } from './defs/receipt';
import { URLParams } from '../defs/endpoint-methods';
declare class DocumentEndpoint extends SaveDocumentResource implements DocumentEndpointMethods {
    get: (urlParams: URLParams<DocumentURLParams>, options?: GetDocumentQueryParams) => Promise<DocumentApiResult>;
    put: (urlParams: URLParams<DocumentURLParams>, document: DocumentPayload<DocumentUpdateData>) => Promise<ReceiptApiResult>;
}
export default DocumentEndpoint;
