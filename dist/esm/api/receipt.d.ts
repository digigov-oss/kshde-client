import Resource from './utils/resource';
import { ReceiptApiResult, ReceiptEndpointMethods, ReceiptURLParams } from './defs/receipt';
import { URLParams } from '../defs/endpoint-methods';
declare class ReceiptEndpoint extends Resource implements ReceiptEndpointMethods {
    get: (urlParams: URLParams<ReceiptURLParams>) => Promise<ReceiptApiResult>;
}
export default ReceiptEndpoint;
