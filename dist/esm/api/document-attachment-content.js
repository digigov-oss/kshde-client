import Resource from './utils/resource';
class DocumentAttachmentContentEndpoint extends Resource {
    get = async (urlParams) => {
        const url = this.toActualUrl(urlParams);
        const response = await this._get({ url, isBlob: true });
        return response.data;
    };
}
export default DocumentAttachmentContentEndpoint;
