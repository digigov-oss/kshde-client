import NavigatableApiResult from './utils/navigatable-api-result';
import SaveDocumentResource from './utils/save-document';
class DocumentAttachmentsEndpoint extends SaveDocumentResource {
    get = async (urlParams) => {
        const url = this.toActualUrl(urlParams);
        const get = async (params) => {
            const response = await this._get({ url, params });
            return response.data;
        };
        const firstPage = await new NavigatableApiResult(get).init({});
        return firstPage.data ? firstPage.data : [];
    };
    post = async (urlParams, attachment) => {
        const url = this.toActualUrl(urlParams);
        return this.saveDocument(url, attachment);
    };
}
export default DocumentAttachmentsEndpoint;
