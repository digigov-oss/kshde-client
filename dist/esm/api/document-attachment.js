import Resource from './utils/resource';
class DocumentAttachmentEndpoint extends Resource {
    get = async (urlParams) => {
        const url = this.toActualUrl(urlParams);
        const response = await this._get({ url });
        return response.data;
    };
}
export default DocumentAttachmentEndpoint;
