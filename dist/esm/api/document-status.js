import NavigatableApiResult from './utils/navigatable-api-result';
import Resource from './utils/resource';
class DocumentStatusEndpoint extends Resource {
    get = async (urlParams, options) => {
        const url = this.toActualUrl(urlParams);
        const get = async (params) => {
            const response = await this._get({ url, params });
            return response.data;
        };
        const params = {
            version: options?.version?.toString(),
            sectorCode: options?.sectorCode
        };
        return new NavigatableApiResult(get).init({
            params,
            page: 1,
            limit: 100
        });
    };
    put = async (urlParams, { version, data }) => {
        const url = this.toActualUrl(urlParams);
        const params = new URLSearchParams({ version: version.toString() });
        const response = await this._put({ url, params, data });
        return response.data;
    };
}
export default DocumentStatusEndpoint;
