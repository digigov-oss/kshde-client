import Resource from './utils/resource';
import NavigatableApiResult from './utils/navigatable-api-result';
class NotificationsEndpoint extends Resource {
    get = async (urlParams, options) => {
        const url = this.toActualUrl(urlParams);
        const get = async (params) => {
            const response = await this._get({ url, params });
            return response.data;
        };
        const params = {
            status: options?.status?.toString(),
            documentProtocolNo: options?.documentProtocolNo,
            type: options?.type?.toString(),
            dateFrom: options?.dateFrom?.toISOString(),
            dateTo: options?.dateTo?.toISOString()
        };
        return new NavigatableApiResult(get).init({
            params,
            page: options?.page,
            limit: options?.limit
        });
    };
}
export default NotificationsEndpoint;
