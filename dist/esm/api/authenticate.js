import Resource from './utils/resource';
class AuthenticateEndpoint extends Resource {
    post = async (urlParams, data) => {
        const url = this.toActualUrl(urlParams);
        const response = await this._post({ url, data });
        return response.data;
    };
}
export default AuthenticateEndpoint;
