import Resource from './utils/resource';
class DocumentContentEndpoint extends Resource {
    get = async (urlParams, options) => {
        const url = this.toActualUrl(urlParams);
        const params = new URLSearchParams();
        if (options?.version) {
            params.append('version', options.version.toString());
        }
        const response = await this._get({ url, params, isBlob: true });
        return response.data;
    };
}
export default DocumentContentEndpoint;
