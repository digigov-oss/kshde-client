import NavigatableApiResult from './utils/navigatable-api-result';
import Resource from './utils/resource';
import { DocumentStatusApiResult } from './defs/document-status';
import { ReceiptApiResult } from './defs/receipt';
import { DocumentStatusEndpointMethods, GetDocumentStatusQueryParams, PutDocumentStatusData } from './defs/document-status';
import { URLParams } from '../defs/endpoint-methods';
import { DocumentURLParams } from './defs';
declare class DocumentStatusEndpoint extends Resource implements DocumentStatusEndpointMethods {
    get: (urlParams: URLParams<DocumentURLParams>, options?: GetDocumentStatusQueryParams) => Promise<NavigatableApiResult<DocumentStatusApiResult>>;
    put: (urlParams: URLParams<DocumentURLParams>, { version, data }: PutDocumentStatusData) => Promise<ReceiptApiResult>;
}
export default DocumentStatusEndpoint;
