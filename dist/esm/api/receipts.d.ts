import Resource from './utils/resource';
import NavigatableApiResult from './utils/navigatable-api-result';
import { GetReceiptsQueryParams, ReceiptApiResult, ReceiptCreateData, ReceiptsEndpointMethods } from './defs/receipt';
import { NoUrlParams } from '../defs/endpoint-methods';
declare class ReceiptsEndpoint extends Resource implements ReceiptsEndpointMethods {
    get: (urlParams: NoUrlParams, options?: GetReceiptsQueryParams) => Promise<NavigatableApiResult<ReceiptApiResult>>;
    post: (urlParams: NoUrlParams, data: ReceiptCreateData) => Promise<ReceiptApiResult>;
}
export default ReceiptsEndpoint;
