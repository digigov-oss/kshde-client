import Resource from './utils/resource';
import { OrgChartEndpointMethods, OrgChartPayload, OrgchartNode } from './defs/orgchart';
import { OrgchartApiResult } from './defs/orgchart';
declare class OrgChartEndpoint extends Resource implements OrgChartEndpointMethods {
    get: () => Promise<OrgchartApiResult>;
    put: (organizationChart: OrgChartPayload) => Promise<OrgchartNode>;
}
export default OrgChartEndpoint;
