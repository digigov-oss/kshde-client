import NavigatableApiResult from './utils/navigatable-api-result';
import { DocumentApiResult, DocumentCreateData, DocumentPayload } from './defs/document';
import { ReceiptApiResult } from './defs/receipt';
import { DocumentsEndpointMethods, GetDocumentsQueryParams } from './defs/document';
import { NoUrlParams } from '../defs/endpoint-methods';
import SaveDocumentResource from './utils/save-document';
declare class DocumentsEndpoint extends SaveDocumentResource implements DocumentsEndpointMethods {
    get: (urlParams: NoUrlParams, options?: GetDocumentsQueryParams) => Promise<NavigatableApiResult<DocumentApiResult>>;
    post: (urlParams: NoUrlParams, document: DocumentPayload<DocumentCreateData>) => Promise<ReceiptApiResult>;
}
export default DocumentsEndpoint;
