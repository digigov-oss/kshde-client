export var DocumentCategory;
(function (DocumentCategory) {
    DocumentCategory[DocumentCategory["Decision"] = 1] = "Decision";
    DocumentCategory[DocumentCategory["Contract"] = 2] = "Contract";
    DocumentCategory[DocumentCategory["Account"] = 3] = "Account";
    DocumentCategory[DocumentCategory["Announcement"] = 4] = "Announcement";
    DocumentCategory[DocumentCategory["Other"] = 5] = "Other";
})(DocumentCategory || (DocumentCategory = {}));
export var DocumentClasification;
(function (DocumentClasification) {
    DocumentClasification[DocumentClasification["General"] = 0] = "General";
    DocumentClasification[DocumentClasification["Confidential"] = 1] = "Confidential";
})(DocumentClasification || (DocumentClasification = {}));
export var RelatedDocumentType;
(function (RelatedDocumentType) {
    RelatedDocumentType[RelatedDocumentType["None"] = 0] = "None";
    RelatedDocumentType[RelatedDocumentType["General"] = 1] = "General";
    RelatedDocumentType[RelatedDocumentType["Answer"] = 2] = "Answer";
})(RelatedDocumentType || (RelatedDocumentType = {}));
//endpoint: documents
export var GetDocumentsStatusOptions;
(function (GetDocumentsStatusOptions) {
    GetDocumentsStatusOptions[GetDocumentsStatusOptions["New"] = 0] = "New";
    GetDocumentsStatusOptions[GetDocumentsStatusOptions["Received"] = 1] = "Received";
    GetDocumentsStatusOptions[GetDocumentsStatusOptions["Sent"] = 2] = "Sent";
})(GetDocumentsStatusOptions || (GetDocumentsStatusOptions = {}));
