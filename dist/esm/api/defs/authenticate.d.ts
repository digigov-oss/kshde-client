import { Post, NoUrlParams } from '../../defs/endpoint-methods';
export interface AuthApiPayload {
    SectorCode: number;
    ClientId: string;
    ClientSecret: string;
}
export interface AuthApiResult {
    AccessToken: string;
    TokenType: string;
    ExpiresOn: string;
}
export type AuthenticateEndpointMethods = Post<AuthApiResult, AuthApiPayload, NoUrlParams>;
