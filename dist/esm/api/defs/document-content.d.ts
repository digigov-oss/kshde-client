/// <reference types="node" />
import { Readable } from 'stream';
import { Get } from '../../defs/endpoint-methods';
import { DocumentURLParams } from './document';
export interface GetDocumentContentQueryParams {
    version?: number;
}
export type DocumentContentEndpointMethods = Get<Readable, GetDocumentContentQueryParams, DocumentURLParams>;
