import { PaginationQueryParams } from '../../defs/pagination-query-params';
import { Link } from './link';
import NavigatableApiResult from '../utils/navigatable-api-result';
import { Get, NoUrlParams } from '../../defs/endpoint-methods';
export declare enum NotificationType {
    DocumentNew = 1,
    DocumentChanged = 2,
    DocumentStatusChanged = 3,
    DocumentReceived = 4
}
export interface NotificationApiResult {
    NotificationId: string;
    Type: NotificationType;
    SenderSectorCode: number;
    RecipientSectorCode: number;
    DocumentProtocolNo: string;
    VersionNumber: number;
    DateCreated: string;
    MetadataJson?: string;
    Links: Link[];
}
export declare enum GetNotificationsStatus {
    NotReceived = 0,
    Received = 1
}
export type GetNotificationsQueryParams = {
    status?: GetNotificationsStatus;
    documentProtocolNo?: string;
    type?: NotificationType;
    dateFrom?: Date;
    dateTo?: Date;
} & PaginationQueryParams;
export type NotificationsEndpointMethods = Get<NavigatableApiResult<NotificationApiResult>, GetNotificationsQueryParams, NoUrlParams>;
