export interface PagedApiResult<T> {
    results: Array<T>;
    NextPage: string | null;
    PreviousPage: string | null;
}
