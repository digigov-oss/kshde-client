import SaveDocumentResource from './utils/save-document';
class DocumentEndpoint extends SaveDocumentResource {
    get = async (urlParams, options) => {
        const url = this.toActualUrl(urlParams);
        const params = new URLSearchParams();
        if (options?.version) {
            params.append('version', options.version.toString());
        }
        const response = await this._get({ url, params });
        return response.data;
    };
    put = async (urlParams, document) => {
        const url = this.toActualUrl(urlParams);
        if (!document.metadata.RecipientSectorCodes.length) {
            throw new Error('No RecipientSectorCodes given');
        }
        return await this.saveDocument(url, document, true);
    };
}
export default DocumentEndpoint;
