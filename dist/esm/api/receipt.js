import Resource from './utils/resource';
class ReceiptEndpoint extends Resource {
    get = async (urlParams) => {
        const url = this.toActualUrl(urlParams);
        const response = await this._get({ url: url });
        return response.data;
    };
}
export default ReceiptEndpoint;
