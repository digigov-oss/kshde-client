import { AuthApiConfig, Config } from './defs/config';
import { AuthApiResult } from './api/defs/authenticate';
import AuthenticateEndpoint from './api/authenticate';

const API_BASE_URL_PRODUCTION = 'https://sddd.mindigital-shde.gr/api/v1';
const API_BASE_URL_DEVELOPMENT = 'https://sdddsp.mindigital-shde.gr/api/v1';

import Utils from './client/utils/utils';

import DocumentsClientApi from './client/defs/documents';
import NotificationsClientApi from './client/defs/notifications';
import OrgchartClientApi from './client/defs/orgchart';
import ReceiptsClientApi from './client/defs/receipts';
import {
    createDocumentsClientApi,
    createNotificationsClientApi,
    createOrgchartClientApi,
    createReceiptsClientApi
} from './client';
import EndpointUrl from './api/defs/endpoint-url';
import { ResourceProps } from './api/utils/resource';

class KSHDEClient {
    private authConfig: AuthApiConfig;
    private authenticateEndpoint: AuthenticateEndpoint;
    private authData?: AuthApiResult;

    public documents: DocumentsClientApi;
    public receipts: ReceiptsClientApi;
    public orgchart: OrgchartClientApi;
    public notifications: NotificationsClientApi;
    public utils: Utils;

    constructor(config: Config) {
        const baseApiURL =
            config.mode === 'production' ? API_BASE_URL_PRODUCTION : API_BASE_URL_DEVELOPMENT;
        this.authConfig = {
            sectorCode: config.sectorCode,
            clientId: config.clientId,
            clientSecret: config.clientSecret
        };
        this.authenticateEndpoint = new AuthenticateEndpoint(
            { baseApiURL, retryConfig: config.retryConfig },
            EndpointUrl.AUTHENTICATE
        );
        const resourceProps: ResourceProps = {
            baseApiURL,
            authenticate: this.authenticate,
            retryConfig: config.retryConfig
        };
        this.orgchart = createOrgchartClientApi(resourceProps);
        this.documents = createDocumentsClientApi(resourceProps);
        this.notifications = createNotificationsClientApi(resourceProps);
        this.receipts = createReceiptsClientApi(resourceProps);
        this.utils = new Utils(this.orgchart, this.documents);
    }

    public authenticate = async (): Promise<string> => {
        if (!this.authData) {
            return await this._doAuthenticate();
        } else {
            let refreshToken = false;
            try {
                const authExpiresTime = Date.parse(this.authData.ExpiresOn);
                const currTime = new Date().getTime();
                if (currTime >= authExpiresTime) {
                    refreshToken = true;
                }
            } catch (e) {
                refreshToken = true;
            }
            if (refreshToken) {
                return await this._doAuthenticate();
            }
            return this.authData.AccessToken;
        }
    };

    private _doAuthenticate = async (): Promise<string> => {
        const authConfig = this.authConfig;
        return await this.authenticateEndpoint
            .post(
                {},
                {
                    SectorCode: authConfig.sectorCode,
                    ClientId: authConfig.clientId,
                    ClientSecret: authConfig.clientSecret
                }
            )
            .then((authData) => {
                this.authData = authData;
                return authData.AccessToken;
            })
            .catch((e) => {
                this.authData = undefined;
                throw e;
            });
    };
}

export default KSHDEClient;
