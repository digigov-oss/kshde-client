import { IAxiosRetryConfig } from 'axios-retry';
export interface AuthApiConfig {
    sectorCode: number;
    clientId: string;
    clientSecret: string;
}

export interface Config extends AuthApiConfig {
    mode?: 'production' | 'development';
    retryConfig?: IAxiosRetryConfig;
}
