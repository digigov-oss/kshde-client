import SaveDocumentResource from './utils/save-document';
import {
    DocumentApiResult,
    DocumentEndpointMethods,
    DocumentPayload,
    DocumentURLParams,
    DocumentUpdateData,
    GetDocumentQueryParams
} from './defs/document';
import { ReceiptApiResult } from './defs/receipt';
import { URLParams } from '../defs/endpoint-methods';

class DocumentEndpoint extends SaveDocumentResource implements DocumentEndpointMethods {
    get = async (
        urlParams: URLParams<DocumentURLParams>,
        options?: GetDocumentQueryParams
    ): Promise<DocumentApiResult> => {
        const url = this.toActualUrl(urlParams);
        const params = new URLSearchParams();
        if (options?.version) {
            params.append('version', options.version.toString());
        }
        const response = await this._get({ url, params });
        return response.data;
    };

    put = async (
        urlParams: URLParams<DocumentURLParams>,
        document: DocumentPayload<DocumentUpdateData>
    ): Promise<ReceiptApiResult> => {
        const url = this.toActualUrl(urlParams);
        if (!document.metadata.RecipientSectorCodes.length) {
            throw new Error('No RecipientSectorCodes given');
        }
        return await this.saveDocument(url, document, true);
    };
}

export default DocumentEndpoint;
