import { AuthApiPayload, AuthApiResult, AuthenticateEndpointMethods } from './defs/authenticate';
import { NoUrlParams } from '../defs/endpoint-methods';
import Resource from './utils/resource';

class AuthenticateEndpoint extends Resource implements AuthenticateEndpointMethods {
    post = async (urlParams: NoUrlParams, data: AuthApiPayload): Promise<AuthApiResult> => {
        const url = this.toActualUrl(urlParams);
        const response = await this._post({ url, data });
        return response.data;
    };
}

export default AuthenticateEndpoint;
