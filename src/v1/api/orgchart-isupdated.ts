import Resource from './utils/resource';
import { URLParams } from '../defs/endpoint-methods';
import {
    OrgchartIsUpdatedEndpointMethods,
    OrgchartVersionURLParams
} from './defs/orgchart-isupdated';

class OrgchartVersionIsUpdatedEndpoint
    extends Resource
    implements OrgchartIsUpdatedEndpointMethods
{
    get = async (urlParams: URLParams<OrgchartVersionURLParams>): Promise<boolean> => {
        const url = this.toActualUrl(urlParams);
        const response = await this._get({ url });
        return response.data;
    };
}

export default OrgchartVersionIsUpdatedEndpoint;
