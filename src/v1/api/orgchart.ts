import Resource from './utils/resource';
import { OrgChartEndpointMethods, OrgChartPayload, OrgchartNode } from './defs/orgchart';
import { OrgchartApiResult } from './defs/orgchart';

class OrgChartEndpoint extends Resource implements OrgChartEndpointMethods {
    get = async (): Promise<OrgchartApiResult> => {
        const url = this.toActualUrl({});
        const response = await this._get({ url });
        return response.data;
    };

    put = async (organizationChart: OrgChartPayload): Promise<OrgchartNode> => {
        const url = this.toActualUrl({});
        const response = await this._put({ url, data: organizationChart });
        return response.data;
    };
}

export default OrgChartEndpoint;
