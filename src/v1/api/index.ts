export { default as NavigatableApiResult } from './utils/navigatable-api-result';
export * from './utils/navigatable-api-result';

export * from './defs';

export { default as AuthenticateEndpoint } from './authenticate';
export { default as DocumentAttachmentContentEndpoint } from './document-attachment-content';
export { default as DocumentAttachmentEndpoint } from './document-attachment';
export { default as DocumentAttachmentsEndpoint } from './document-attachments';
export { default as DocumentContentEndpoint } from './document-content';
export { default as DocumentStatusEndpoint } from './document-status';
export { default as DocumentEndpoint } from './document';
export { default as DocumentsEndpoint } from './documents';
export { default as NotificationsEndpoint } from './notifications';
export { default as OrgchartVersionIsUpdatedEndpoint } from './orgchart-isupdated';
export { default as OrgChartEndpoint } from './orgchart';
export { default as ReceiptEndpoint } from './receipt';
export { default as ReceiptsEndpoint } from './receipts';
