import { Readable } from 'stream';
import Resource from './utils/resource';
import {
    DocumentContentEndpointMethods,
    GetDocumentContentQueryParams
} from './defs/document-content';
import { URLParams } from '../defs/endpoint-methods';
import { DocumentURLParams } from './defs';
class DocumentContentEndpoint extends Resource implements DocumentContentEndpointMethods {
    get = async (
        urlParams: URLParams<DocumentURLParams>,
        options?: GetDocumentContentQueryParams
    ): Promise<Readable> => {
        const url = this.toActualUrl(urlParams);
        const params = new URLSearchParams();
        if (options?.version) {
            params.append('version', options.version.toString());
        }
        const response = await this._get({ url, params, isBlob: true });
        return response.data;
    };
}

export default DocumentContentEndpoint;
