import axios, { AxiosInstance, AxiosRequestConfig, isAxiosError } from 'axios';
import FormData from 'form-data';
import { URLParams, URLParamsType } from '../../defs';
import EndpointUrl from '../defs/endpoint-url';
import axiosRetry, { IAxiosRetryConfig } from 'axios-retry';

interface PostParams {
    url: string;
    data?: any;
    params?: URLSearchParams;
}

type PutParams = PostParams;

interface SaveParams extends PostParams {
    update?: boolean;
    accessToken?: string;
}
interface GetParams {
    url: string;
    params?: URLSearchParams;
    isBlob?: true;
}
export interface ResourceProps {
    baseApiURL: string;
    authenticate?: () => Promise<string>;
    retryConfig?: IAxiosRetryConfig;
}

const NonAuthenticatedUrls = [EndpointUrl.AUTHENTICATE];

class Resource {
    private baseApiURL: string;
    private authenticate?: () => Promise<string>;
    private endpointUrl: EndpointUrl;
    private shouldAuthenticate: boolean;
    private axiosClient: AxiosInstance;

    constructor(props: ResourceProps, endpointUrl: EndpointUrl) {
        this.authenticate = props.authenticate;
        this.endpointUrl = endpointUrl;
        this.shouldAuthenticate = !NonAuthenticatedUrls.includes(endpointUrl);
        this.baseApiURL = props.baseApiURL;
        this.axiosClient = axios.create();
        if (props.retryConfig) {
            axiosRetry(this.axiosClient, props.retryConfig);
        }
    }

    protected _get = async ({ url, params, isBlob }: GetParams) => {
        if (!this.authenticate) {
            throw Error('No authentication method provided');
        }
        return this.authenticate().then(async (accessToken) => {
            const responseType = isBlob ? 'stream' : undefined;
            const config: AxiosRequestConfig = {
                headers: {
                    Authorization: `Bearer ${accessToken}`,
                    'Cache-Control': 'no-cache'
                },
                params: params,
                responseType: responseType
            };
            try {
                return await this.axiosClient.get(url, config);
            } catch (e) {
                throw this.toError(e);
            }
        });
    };

    protected _post = async ({ url, data, params }: PostParams) => {
        let accessToken;
        if (this.shouldAuthenticate) {
            if (!this.authenticate) {
                throw Error('No authentication method provided');
            }
            accessToken = await this.authenticate();
        }
        return this._save({
            url,
            data,
            params,
            accessToken
        });
    };

    protected _put = async ({ url, data, params }: PutParams) => {
        if (!this.authenticate) {
            throw Error('No authentication method provided');
        }
        const accessToken = await this.authenticate();
        return this._save({
            url,
            data,
            params,
            update: true,
            accessToken
        });
    };

    private _save = async ({ url, data, params, accessToken, update = false }: SaveParams) => {
        const headers: Record<string, string> =
            data instanceof FormData
                ? {
                      ...data.getHeaders()
                  }
                : {};

        const config: AxiosRequestConfig = { headers: headers, params };
        if (accessToken) {
            headers['Authorization'] = `Bearer ${accessToken}`;
        }
        try {
            return update
                ? await this.axiosClient.put(url, data, config)
                : await this.axiosClient.post(url, data, config);
        } catch (e) {
            throw this.toError(e);
        }
    };

    protected toError(error: any): any {
        if (isAxiosError(error)) {
            const response = error.response;
            if (response?.data) {
                return response.data;
            }
        }
        return error;
    }

    protected toActualUrl = <T extends URLParamsType>(params?: URLParams<T>) => {
        return `${this.baseApiURL}/${this.replaceParams(this.endpointUrl, params)}`;
    };

    private replaceParams = <T extends URLParamsType>(
        url: string,
        params?: URLParams<T>
    ): string => {
        if (!params || !Object.keys(params).length) return url;
        let result = url;
        Object.entries(params).forEach(([key, value]) => {
            result = result.replace(`:${key}`, value);
        });
        return result;
    };
}

export default Resource;
