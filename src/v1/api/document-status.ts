import NavigatableApiResult from './utils/navigatable-api-result';
import Resource from './utils/resource';
import { DocumentStatusApiResult } from './defs/document-status';
import { ReceiptApiResult } from './defs/receipt';
import { PagedApiResult } from './defs/paged-result';
import {
    DocumentStatusEndpointMethods,
    GetDocumentStatusQueryParams,
    PutDocumentStatusData
} from './defs/document-status';
import { URLParams } from '../defs/endpoint-methods';
import { DocumentURLParams } from './defs';

class DocumentStatusEndpoint extends Resource implements DocumentStatusEndpointMethods {
    get = async (
        urlParams: URLParams<DocumentURLParams>,
        options?: GetDocumentStatusQueryParams
    ): Promise<NavigatableApiResult<DocumentStatusApiResult>> => {
        const url = this.toActualUrl(urlParams);

        const get = async (
            params: URLSearchParams
        ): Promise<PagedApiResult<DocumentStatusApiResult>> => {
            const response = await this._get({ url, params });
            return response.data;
        };

        const params = {
            version: options?.version?.toString(),
            sectorCode: options?.sectorCode
        };
        return new NavigatableApiResult(get).init({
            params,
            page: 1,
            limit: 100
        });
    };

    put = async (
        urlParams: URLParams<DocumentURLParams>,
        { version, data }: PutDocumentStatusData
    ): Promise<ReceiptApiResult> => {
        const url = this.toActualUrl(urlParams);
        const params = new URLSearchParams({ version: version.toString() });
        const response = await this._put({ url, params, data });
        return response.data;
    };
}

export default DocumentStatusEndpoint;
