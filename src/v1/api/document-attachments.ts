import { PagedApiResult } from './defs/paged-result';

import NavigatableApiResult from './utils/navigatable-api-result';
import {
    DocumentAttachmentApiResult,
    DocumentAttachmentCreateData
} from './defs/document-attachment';

import { DocumentAttachmentsEndpointMethods } from './defs/document-attachment';
import { URLParams } from '../defs/endpoint-methods';
import { DocumentPayload, DocumentURLParams } from './defs';
import SaveDocumentResource from './utils/save-document';

class DocumentAttachmentsEndpoint
    extends SaveDocumentResource
    implements DocumentAttachmentsEndpointMethods
{
    get = async (
        urlParams: URLParams<DocumentURLParams>
    ): Promise<DocumentAttachmentApiResult[]> => {
        const url = this.toActualUrl(urlParams);
        const get = async (
            params: URLSearchParams
        ): Promise<PagedApiResult<DocumentAttachmentApiResult>> => {
            const response = await this._get({ url, params });
            return response.data;
        };
        const firstPage = await new NavigatableApiResult(get).init({});
        return firstPage.data ? firstPage.data : [];
    };

    post = async (
        urlParams: URLParams<DocumentURLParams>,
        attachment: DocumentPayload<DocumentAttachmentCreateData>
    ): Promise<DocumentAttachmentApiResult> => {
        const url = this.toActualUrl(urlParams);
        return this.saveDocument(url, attachment);
    };
}

export default DocumentAttachmentsEndpoint;
