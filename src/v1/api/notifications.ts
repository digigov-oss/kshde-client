import Resource from './utils/resource';
import { PagedApiResult } from './defs/paged-result';
import NavigatableApiResult from './utils/navigatable-api-result';

import {
    GetNotificationsQueryParams,
    NotificationApiResult,
    NotificationsEndpointMethods
} from './defs/notifications';
import { NoUrlParams } from '../defs/endpoint-methods';

class NotificationsEndpoint extends Resource implements NotificationsEndpointMethods {
    get = async (
        urlParams: NoUrlParams,
        options?: GetNotificationsQueryParams
    ): Promise<NavigatableApiResult<NotificationApiResult>> => {
        const url = this.toActualUrl(urlParams);

        const get = async (
            params: URLSearchParams
        ): Promise<PagedApiResult<NotificationApiResult>> => {
            const response = await this._get({ url, params });
            return response.data;
        };
        const params = {
            status: options?.status?.toString(),
            documentProtocolNo: options?.documentProtocolNo,
            type: options?.type?.toString(),
            dateFrom: options?.dateFrom?.toISOString(),
            dateTo: options?.dateTo?.toISOString()
        };
        return new NavigatableApiResult(get).init({
            params,
            page: options?.page,
            limit: options?.limit
        });
    };
}

export default NotificationsEndpoint;
