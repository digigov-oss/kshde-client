import { PagedApiResult } from './defs/paged-result';

import NavigatableApiResult from './utils/navigatable-api-result';
import { DocumentApiResult, DocumentCreateData, DocumentPayload } from './defs/document';
import { ReceiptApiResult } from './defs/receipt';
import { validateFileName } from '../utils/validateFiles';
import { DocumentsEndpointMethods, GetDocumentsQueryParams } from './defs/document';
import { NoUrlParams } from '../defs/endpoint-methods';
import SaveDocumentResource from './utils/save-document';

class DocumentsEndpoint extends SaveDocumentResource implements DocumentsEndpointMethods {
    get = async (
        urlParams: NoUrlParams,
        options?: GetDocumentsQueryParams
    ): Promise<NavigatableApiResult<DocumentApiResult>> => {
        const url = this.toActualUrl(urlParams);

        const get = async (params: URLSearchParams): Promise<PagedApiResult<DocumentApiResult>> => {
            const response = await this._get({ url, params });
            return response.data;
        };

        const params = {
            status: options?.status?.toString(),
            dateFrom: options?.dateFrom?.toISOString(),
            dateTo: options?.dateTo?.toISOString()
        };
        return new NavigatableApiResult(get).init({
            params,
            page: options?.page,
            limit: options?.limit
        });
    };

    post = async (
        urlParams: NoUrlParams,
        document: DocumentPayload<DocumentCreateData>
    ): Promise<ReceiptApiResult> => {
        if (!document.metadata.RecipientSectorCodes.length) {
            throw new Error('No RecipientSectorCodes given');
        }
        validateFileName(document.metadata.FileName);
        const url = this.toActualUrl(urlParams);
        return await this.saveDocument(url, document);
    };
}

export default DocumentsEndpoint;
