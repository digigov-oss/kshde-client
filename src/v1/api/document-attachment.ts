import { URLParams } from '../defs/endpoint-methods';

import {
    DocumentAttachmentApiResult,
    DocumentAttachmentEndpointMethods,
    DocumentAttachmentURLParams
} from './defs/document-attachment';
import Resource from './utils/resource';
class DocumentAttachmentEndpoint extends Resource implements DocumentAttachmentEndpointMethods {
    get = async (
        urlParams: URLParams<DocumentAttachmentURLParams>
    ): Promise<DocumentAttachmentApiResult> => {
        const url = this.toActualUrl(urlParams);
        const response = await this._get({ url });
        return response.data;
    };
}

export default DocumentAttachmentEndpoint;
