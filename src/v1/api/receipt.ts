import Resource from './utils/resource';

import { ReceiptApiResult, ReceiptEndpointMethods, ReceiptURLParams } from './defs/receipt';
import { URLParams } from '../defs/endpoint-methods';

class ReceiptEndpoint extends Resource implements ReceiptEndpointMethods {
    get = async (urlParams: URLParams<ReceiptURLParams>): Promise<ReceiptApiResult> => {
        const url = this.toActualUrl(urlParams);
        const response = await this._get({ url: url });
        return response.data;
    };
}

export default ReceiptEndpoint;
