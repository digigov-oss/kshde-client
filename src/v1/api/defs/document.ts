import { PaginationQueryParams } from '../../defs/pagination-query-params';
import { Link } from './link';
import { Get, Post, Put, NoUrlParams } from '../../defs/endpoint-methods';
import NavigatableApiResult from '../utils/navigatable-api-result';
import { ReceiptApiResult } from './receipt';
import { Readable } from 'stream';

export interface SectorDepartmentResource {
    SectorCode: number;
    DepartmentCode: number;
}

export enum DocumentCategory {
    Decision = 1,
    Contract = 2,
    Account = 3,
    Announcement = 4,
    Other = 5
}
export enum DocumentClasification {
    General = 0,
    Confidential = 1
}

export enum RelatedDocumentType {
    None = 0,
    General = 1,
    Answer = 2
}

export interface VersionData {
    VersionNumber: number;
    VersionDate: string;
    VersionComments?: string;
}

interface FileData {
    FileName?: string;
    Comments?: string;
}
export interface DocumentMetadata extends FileData {
    IsFinal: boolean;
}

export interface DocumentPayload<T> {
    data: Readable;
    metadata: T;
}

export interface DocumentVersion extends VersionData {
    DocumentProtocolNo: string;
    Links: Link[];
}

export interface DocumentData {
    Subject: string;
    RecipientSectorCodes: number[];
    Category?: DocumentCategory;
    Classification?: DocumentClasification;
    AuthorName?: string;
    DueDate?: string;
    DiavgiaId?: string;
    KimdisId?: string;
    MetadataJson?: string;
    RelatedDocumentType?: RelatedDocumentType;
    RelatedDocumentProtocolNo?: string;
    SenderSectorDirectorate?: string;
    SenderSectorDepartment?: string;
    RecipientSectorDepartments?: SectorDepartmentResource[];
    CCSectorCodes?: number[];
    CCSectorDepartments?: SectorDepartmentResource[];
    LocalSectorProtocolNo: string;
    LocalSectorProtocolDate: string;
}

export type DocumentURLParams = { documentProtocolNo: string };

export interface DocumentCreateData extends DocumentData, DocumentMetadata {
    FileName: string; //Override FileMetadataPayload.FileName
    OrgChartVersion: number;
    VersionComments?: string;
}

export interface DocumentUpdateData
    extends Omit<DocumentData, 'Subject'>,
    Omit<DocumentMetadata, 'FileName'> {
    Subject?: string;
    OrgChartVersion: number;
    VersionComments: string;
}

export interface DocumentApiResult extends DocumentData, VersionData, FileData {
    ProtocolNo: string;
    ProtocolDate: string;
    IsLatestVersion: boolean;
    IsPasswordProtected: "True" | "False" | null;
    SenderSectorCode: number;
    Hash?: string;
    PreviousVersions?: Array<DocumentVersion>;
}

//endpoint: documents

export enum GetDocumentsStatusOptions {
    New = 0,
    Received = 1,
    Sent = 2
}

export type GetDocumentsQueryParams = {
    status?: GetDocumentsStatusOptions;
    dateFrom?: Date;
    dateTo?: Date;
} & PaginationQueryParams;

export type DocumentsEndpointMethods = Get<
    NavigatableApiResult<DocumentApiResult>,
    GetDocumentsQueryParams,
    NoUrlParams
> &
    Post<ReceiptApiResult, DocumentPayload<DocumentCreateData>, NoUrlParams>;

//endpoint: documents/{protocolNo}

export interface GetDocumentQueryParams {
    version?: number;
}

export type DocumentEndpointMethods = Get<
    DocumentApiResult,
    GetDocumentQueryParams,
    DocumentURLParams
> &
    Put<ReceiptApiResult, DocumentPayload<DocumentUpdateData>, DocumentURLParams>;
