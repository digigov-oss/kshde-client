import { Get } from '../../defs/endpoint-methods';

//Flattened version of the OrgChart
export type OrgchartNode = {
    Code: number;
    Name: string;
    NameEnglish: string | null;
    IsActive: boolean;
    IsSDDDNode: boolean;
    Departments?: OrgchartNode[];
    ChildNodes?: OrgchartNode[];
};

export type OrgchartApiResult = {
    Version: number;
    RootNode: OrgchartNode;
};

export type OrgChartPayload = {
    Departments: OrgchartNode[];
};

//endpoint: orgchart
export type OrgChartEndpointMethods = Get<OrgchartApiResult, void, never>;
