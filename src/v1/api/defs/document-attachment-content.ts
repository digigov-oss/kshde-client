import { Get } from '../../defs/endpoint-methods';
import { Readable } from 'stream';
import { DocumentAttachmentURLParams } from './document-attachment';

//endpoint: documents/{protocolNo}/attachments/content
export type DocumentAttachmentContentEndpointMethods = Get<
    Readable,
    never,
    DocumentAttachmentURLParams
>;
