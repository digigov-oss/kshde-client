import { PaginationQueryParams } from '../../defs/pagination-query-params';
import { Get, Post, NoUrlParams } from '../../defs/endpoint-methods';
import { Link } from './link';
import NavigatableApiResult from '../utils/navigatable-api-result';

export enum ReceiptType {
    NewDocument = 1,
    ChangeDocument = 2,
    ChangeDocumentStatus = 3,
    DocumentReceived = 4,
    DocumentSent = 5
}

export interface ReceiptCreateData {
    ReceiptDate: string;
    DocumentProtocolNo: string;
    VersionNumber: number;
    LocalReceiptId: string;
}

export type ReceiptData = ReceiptCreateData & {
    Type: ReceiptType;
};

export interface ReceiptApiResult extends ReceiptData {
    ReceiptId: string;
    OriginatorId: number;
    RecipientId: number;
    Links: Link[];
}

export type ReceiptURLParams = {
    receiptId: string;
};
//endpoint: receipts/{receiptId}
export type ReceiptEndpointMethods = Get<ReceiptApiResult, never, ReceiptURLParams>;

//endpoint: receipts
export type GetReceiptsQueryParams = {
    documentProtocolNo?: string;
    type?: ReceiptType;
    dateFrom?: Date;
    dateTo?: Date;
} & PaginationQueryParams;

export type ReceiptsEndpointMethods = Get<
    NavigatableApiResult<ReceiptApiResult>,
    GetReceiptsQueryParams,
    NoUrlParams
> &
    Post<ReceiptApiResult, ReceiptCreateData, NoUrlParams>;
