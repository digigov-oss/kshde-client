import { Get, URLParams } from '../../defs/endpoint-methods';

//endpoint: orgchart/{currentVersion}/isupdated

export type OrgchartVersionURLParams = { currentVersion: number };

export type OrgchartIsUpdatedEndpointMethods = Get<
    boolean,
    void,
    URLParams<OrgchartVersionURLParams>
>;
