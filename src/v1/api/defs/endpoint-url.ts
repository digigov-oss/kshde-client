enum EndpointUrl {
    AUTHENTICATE = 'authenticate',
    DOCUMENTS = 'documents',
    DOCUMENT = 'documents/:documentProtocolNo',
    DOCUMENT_CONTENT = 'documents/:documentProtocolNo/content',
    DOCUMENT_STATUS = 'documents/:documentProtocolNo/status',

    DOCUMENT_ATTACHMENTS = 'documents/:documentProtocolNo/attachments',
    DOCUMENT_ATTACHMENT = 'documents/:documentProtocolNo/attachments/:attachmentId',
    DOCUMENT_ATTACHMENT_CONTENT = 'documents/:documentProtocolNo/attachments/:attachmentId/content',

    NOTIFICATIONS = 'notifications',
    ORCHART = 'orgchart',
    ORGCHART_ISUPDATED = `orgchart/:currentVersion/isupdated`,
    RECEIPTS = `receipts`,
    RECEIPT = `receipts/:receiptId`
}
export default EndpointUrl;
