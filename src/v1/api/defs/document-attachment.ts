import { Get, Post } from '../../defs/endpoint-methods';
import { Link } from './link';
import { DocumentMetadata, DocumentPayload, DocumentURLParams } from './document';
export interface DocumentAttachmentCreateData extends DocumentMetadata {
    FileName: string; //Override FileMetadataPayload.FileName
}
export interface DocumentAttachmentApiResult {
    AttachmentId: string;
    DocumentProtocolNo: string;
    IsPasswordProtected: "True" | "False" | null;
    VersionNumber: number;
    FileName: string;
    DateReceived: string;
    Comments?: string;
    Hash?: string;
    IsFinal?: boolean;
    Links?: Link[];
}

export type DocumentAttachmentURLParams = {
    documentProtocolNo: string;
    attachmentId: string;
};

//endpoint: documents/{protocolNo}/attachments/{attachmentId}
export type DocumentAttachmentEndpointMethods = Get<
    DocumentAttachmentApiResult,
    never,
    DocumentAttachmentURLParams
>;

//endpoint: documents/{protocolNo}/attachments
export type DocumentAttachmentsEndpointMethods = Get<
    DocumentAttachmentApiResult[],
    never,
    DocumentURLParams
> &
    Post<
        DocumentAttachmentApiResult,
        DocumentPayload<DocumentAttachmentCreateData>,
        DocumentURLParams
    >;
