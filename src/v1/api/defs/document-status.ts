import NavigatableApiResult from '../utils/navigatable-api-result';
import { Get, Put } from '../../defs/endpoint-methods';
import { Link } from './link';
import { ReceiptApiResult } from './receipt';
import { DocumentURLParams } from './document';

export enum DocumentStatus {
    Received = 0,
    Open = 1,
    Finalized = 2,
    Filed = 3,
    ToBeCharged = 4,
    Processed = 5,
    ToBeAnswered = 6,
    ToBeSigned = 7
}

export interface DocumentStatusData {
    Status: DocumentStatus;
    LocalProtocolNo: string;
    DepartmentCode?: number;
    Comments?: string;
    WorkflowStage?: string;
    WorkflowXml?: string;
}

export type DocumentStatusUpdateData = DocumentStatusData;

export interface DocumentStatusApiResult extends DocumentStatusData {
    SectorCode: number;
    VersionNumber: number;
    DocumentProtocolNo: string;
    DateChanged: string;
    Links?: Link[];
}

export interface GetDocumentStatusQueryParams {
    version?: number;
    sectorCode?: string;
}

export interface PutDocumentStatusData {
    version: number;
    data: DocumentStatusUpdateData;
}

export type DocumentStatusEndpointMethods = Get<
    NavigatableApiResult<DocumentStatusApiResult>,
    GetDocumentStatusQueryParams,
    DocumentURLParams
> &
    Put<ReceiptApiResult, PutDocumentStatusData, DocumentURLParams>;
