export interface Link {
    Rel?: string;
    Href?: string;
    Method?: string;
}
