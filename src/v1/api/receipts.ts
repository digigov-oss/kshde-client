import Resource from './utils/resource';
import { PagedApiResult } from './defs/paged-result';
import NavigatableApiResult from './utils/navigatable-api-result';
import {
    GetReceiptsQueryParams,
    ReceiptApiResult,
    ReceiptCreateData,
    ReceiptsEndpointMethods
} from './defs/receipt';
import { NoUrlParams } from '../defs/endpoint-methods';

class ReceiptsEndpoint extends Resource implements ReceiptsEndpointMethods {
    get = async (
        urlParams: NoUrlParams,
        options?: GetReceiptsQueryParams
    ): Promise<NavigatableApiResult<ReceiptApiResult>> => {
        const url = this.toActualUrl(urlParams);

        const get = async (params: URLSearchParams): Promise<PagedApiResult<ReceiptApiResult>> => {
            const response = await this._get({ url, params });
            return response.data;
        };

        const params = {
            documentProtocolNo: options?.documentProtocolNo,
            type: options?.type?.toString(),
            dateFrom: options?.dateFrom?.toISOString(),
            dateTo: options?.dateTo?.toISOString()
        };
        return new NavigatableApiResult(get).init({
            params,
            page: options?.page,
            limit: options?.limit
        });
    };

    post = async (urlParams: NoUrlParams, data: ReceiptCreateData): Promise<ReceiptApiResult> => {
        const url = this.toActualUrl(urlParams);
        const response = await this._post({ url, data });
        return response.data;
    };
}

export default ReceiptsEndpoint;
