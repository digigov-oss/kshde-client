import { Readable } from 'stream';
import Resource from './utils/resource';
import { DocumentAttachmentContentEndpointMethods } from './defs/document-attachment-content';
import { URLParams } from '../defs/endpoint-methods';
import { DocumentAttachmentURLParams } from './defs';

class DocumentAttachmentContentEndpoint
    extends Resource
    implements DocumentAttachmentContentEndpointMethods
{
    get = async (urlParams: URLParams<DocumentAttachmentURLParams>): Promise<Readable> => {
        const url = this.toActualUrl(urlParams);
        const response = await this._get({ url, isBlob: true });
        return response.data;
    };
}

export default DocumentAttachmentContentEndpoint;
