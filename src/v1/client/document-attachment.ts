import DocumentAttachmentContentEndpoint from '../api/document-attachment-content';

import DocumentAttachmentEndpoint from '../api/document-attachment';
import DocumentAttachmentClientApi from './defs/document-attachment';
import createDocumentAttachmentContentClientApi from './document-attachment-content';

const createDocumentAttachmentClientApi = ({
    documentProtocolNo,
    attachmentId,
    documentAttachmentEndpoint,
    documentAttachmentContentEndpoint
}: {
    documentProtocolNo: string;
    attachmentId: string;
    documentAttachmentEndpoint: DocumentAttachmentEndpoint;
    documentAttachmentContentEndpoint: DocumentAttachmentContentEndpoint;
}): DocumentAttachmentClientApi => {
    const urlParams = { documentProtocolNo: documentProtocolNo, attachmentId: attachmentId };
    const documentAttachmentContent = createDocumentAttachmentContentClientApi({
        ...urlParams,
        documentAttachmentContentEndpoint: documentAttachmentContentEndpoint
    });
    return {
        content: documentAttachmentContent,
        get: () => {
            return documentAttachmentEndpoint.get(urlParams);
        }
    };
};
export default createDocumentAttachmentClientApi;
