import DocumentEndpoint from '../api/document';
import { DocumentPayload, DocumentUpdateData, GetDocumentQueryParams } from '../api/defs';
import DocumentClientApi from './defs/document';
import DocumentStatusEndpoint from 'src/v1/api/document-status';
import DocumentContentEndpoint from 'src/v1/api/document-content';
import DocumentAttachmentsEndpoint from 'src/v1/api/document-attachments';
import DocumentAttachmentEndpoint from 'src/v1/api/document-attachment';
import DocumentAttachmentContentEndpoint from 'src/v1/api/document-attachment-content';
import createDocumentStatusClientApi from './document-status';
import createDocumentContentClientApi from './document-content';
import createDocumentAttachmentsClientApi from './document-attachments';

interface Params {
    documentProtocolNo: string;
    documentEndpoint: DocumentEndpoint;
    documentStatusEndpoint: DocumentStatusEndpoint;
    documentContentEndpoint: DocumentContentEndpoint;
    documentAttachmentsEndpoint: DocumentAttachmentsEndpoint;
    documentAttachmentEndpoint: DocumentAttachmentEndpoint;
    documentAttachmentContentEndpoint: DocumentAttachmentContentEndpoint;
}

const createDocumentClientApi = ({
    documentProtocolNo,
    documentEndpoint,
    documentStatusEndpoint,
    documentContentEndpoint,
    documentAttachmentsEndpoint,
    documentAttachmentEndpoint,
    documentAttachmentContentEndpoint
}: Params): DocumentClientApi => {
    const urlParams = { documentProtocolNo: documentProtocolNo };

    const documentStatus = createDocumentStatusClientApi(
        documentProtocolNo,
        documentStatusEndpoint
    );
    const documentContent = createDocumentContentClientApi(
        documentProtocolNo,
        documentContentEndpoint
    );
    const documentAttachments = createDocumentAttachmentsClientApi({
        documentProtocolNo: documentProtocolNo,
        documentAttachmentsEndpoint: documentAttachmentsEndpoint,
        documentAttachmentEndpoint: documentAttachmentEndpoint,
        documentAttachmentContentEndpoint: documentAttachmentContentEndpoint
    });

    const documentApi: DocumentClientApi = {
        attachments: documentAttachments,
        content: documentContent,
        status: documentStatus,

        get: (queryParams?: GetDocumentQueryParams) => {
            return documentEndpoint.get(urlParams, queryParams);
        },
        put: (payload: DocumentPayload<DocumentUpdateData>) => {
            return documentEndpoint.put(urlParams, payload);
        }
    };

    return documentApi;
};
export default createDocumentClientApi;
