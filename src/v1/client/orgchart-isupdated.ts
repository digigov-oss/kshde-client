import OrgchartIsUpdatedEndpoint from '../api/orgchart-isupdated';
import OrgchartIsUpdatedClientApi from './defs/orgchart-isupdated';

const createOrgchartIsUpdatedClientApi = (
    currentVersion: number,
    orgchartIsUpdatedEndpoint: OrgchartIsUpdatedEndpoint
): OrgchartIsUpdatedClientApi => {
    return {
        get: async () => {
            return orgchartIsUpdatedEndpoint.get({ currentVersion: currentVersion.toString() });
        }
    };
};

export default createOrgchartIsUpdatedClientApi;
