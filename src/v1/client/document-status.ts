import { GetDocumentStatusQueryParams, PutDocumentStatusData } from '../api/defs';
import DocumentStatusEndpoint from '../api/document-status';
import DocumentStatusClientApi from './defs/document-status';

const createDocumentStatusClientApi = (
    documentProtocolNo: string,
    documentStatusEndpoint: DocumentStatusEndpoint
): DocumentStatusClientApi => {
    const urlParams = { documentProtocolNo: documentProtocolNo };
    return {
        get: (queryParams?: GetDocumentStatusQueryParams) => {
            return documentStatusEndpoint.get(urlParams, queryParams);
        },
        put: (payload: PutDocumentStatusData) => {
            return documentStatusEndpoint.put(urlParams, payload);
        }
    };
};

export default createDocumentStatusClientApi;
