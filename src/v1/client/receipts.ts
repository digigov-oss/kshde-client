import { GetReceiptsQueryParams, ReceiptCreateData } from '../api/defs/receipt';
import ReceiptEndpoint from '../api/receipt';
import ReceiptsEndpoint from '../api/receipts';
import ReceiptsClientApi from './defs/receipts';
import ReceiptClientApi from './defs/receipt';
import createReceiptClientApi from './receipt';
import { ResourceProps } from '../api/utils/resource';
import EndpointUrl from '../api/defs/endpoint-url';

const createReceiptsClientApi = (resourceProps: ResourceProps): ReceiptsClientApi => {
    const receipts = new ReceiptsEndpoint(resourceProps, EndpointUrl.RECEIPTS);
    const receipt = new ReceiptEndpoint(resourceProps, EndpointUrl.RECEIPT);
    function f(receiptId: string): ReceiptClientApi {
        return createReceiptClientApi(receiptId, receipt);
    }
    f.get = (queryParams?: GetReceiptsQueryParams) => {
        return receipts.get({}, queryParams);
    };
    f.post = (receipt: ReceiptCreateData) => {
        return receipts.post({}, receipt);
    };
    return f;
};

export default createReceiptsClientApi;
