import DocumentAttachmentContentEndpoint from '../api/document-attachment-content';
import DocumentAttachmentContentClientApi from './defs/document-attachment-content';

const createDocumentAttachmentContentClientApi = ({
    documentProtocolNo,
    attachmentId,
    documentAttachmentContentEndpoint
}: {
    documentProtocolNo: string;
    attachmentId: string;
    documentAttachmentContentEndpoint: DocumentAttachmentContentEndpoint;
}): DocumentAttachmentContentClientApi => {
    const urlParams = { documentProtocolNo: documentProtocolNo, attachmentId: attachmentId };
    return {
        get: () => {
            return documentAttachmentContentEndpoint.get(urlParams);
        }
    };
};

export default createDocumentAttachmentContentClientApi;
