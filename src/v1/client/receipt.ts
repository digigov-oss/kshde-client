import ReceiptEndpoint from '../api/receipt';
import ReceiptClientApi from './defs/receipt';

const createReceiptClientApi = (
    receiptId: string,
    receiptEndpoint: ReceiptEndpoint
): ReceiptClientApi => {
    return {
        get: () => {
            return receiptEndpoint.get({ receiptId: receiptId });
        }
    };
};

export default createReceiptClientApi;
