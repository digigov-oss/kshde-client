import OrgchartIsUpdatedEndpoint from '../api/orgchart-isupdated';
import OrgChartEndpoint from '../api/orgchart';
import OrgchartClientApi from './defs/orgchart';
import OrgchartVersionClientApi from './defs/orgchart-version';
import createOrgchartVersionClientApi from './orgchart-version';
import { ResourceProps } from '../api/utils/resource';
import EndpointUrl from '../api/defs/endpoint-url';

const OrgChart = (resourceProps: ResourceProps): OrgchartClientApi => {
    const orgchart = new OrgChartEndpoint(resourceProps, EndpointUrl.ORCHART);
    const orgchartIsUpdated = new OrgchartIsUpdatedEndpoint(
        resourceProps,
        EndpointUrl.ORGCHART_ISUPDATED
    );
    function f(currentVersion: number): OrgchartVersionClientApi {
        return createOrgchartVersionClientApi(currentVersion, orgchartIsUpdated);
    }
    f.get = orgchart.get;
    f.put = orgchart.put;
    return f;
};

export default OrgChart;
