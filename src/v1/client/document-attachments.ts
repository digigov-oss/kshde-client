import DocumentAttachmentContentEndpoint from '../api/document-attachment-content';
import DocumentAttachmentEndpoint from '../api/document-attachment';
import DocumentAttachmentsEndpoint from '../api/document-attachments';
import { DocumentAttachmentCreateData, DocumentPayload } from '../api/defs';
import DocumentAttachmentsClientApi from './defs/document-attachments';
import DocumentAttachmentClientApi from './defs/document-attachment';
import createDocumentAttachmentClientApi from './document-attachment';

const createDocumentAttachmentsClientApi = ({
    documentProtocolNo,
    documentAttachmentsEndpoint,
    documentAttachmentEndpoint,
    documentAttachmentContentEndpoint
}: {
    documentProtocolNo: string;
    documentAttachmentsEndpoint: DocumentAttachmentsEndpoint;
    documentAttachmentEndpoint: DocumentAttachmentEndpoint;
    documentAttachmentContentEndpoint: DocumentAttachmentContentEndpoint;
}): DocumentAttachmentsClientApi => {
    function f(attachmentId: string): DocumentAttachmentClientApi {
        return createDocumentAttachmentClientApi({
            documentProtocolNo,
            attachmentId,
            documentAttachmentEndpoint,
            documentAttachmentContentEndpoint
        });
    }

    const urlParams = { documentProtocolNo: documentProtocolNo };
    f.get = () => {
        return documentAttachmentsEndpoint.get(urlParams);
    };
    f.post = (attachment: DocumentPayload<DocumentAttachmentCreateData>) => {
        return documentAttachmentsEndpoint.post(urlParams, attachment);
    };
    return f;
};
export default createDocumentAttachmentsClientApi;
