import { ReceiptApiResult } from '../../api/defs/receipt';
import {
    DocumentApiResult,
    DocumentPayload,
    DocumentUpdateData,
    GetDocumentQueryParams
} from '../../api/defs';
import DocumentAttachmentsClientApi from './document-attachments';
import DocumentContentClientApi from './document-content';
import DocumentStatusClientApi from './document-status';

type DocumentClientApi = {
    get: (queryParams?: GetDocumentQueryParams) => Promise<DocumentApiResult>;
    put: (payload: DocumentPayload<DocumentUpdateData>) => Promise<ReceiptApiResult>;
    content: DocumentContentClientApi;
    attachments: DocumentAttachmentsClientApi;
    status: DocumentStatusClientApi;
};

export default DocumentClientApi;
