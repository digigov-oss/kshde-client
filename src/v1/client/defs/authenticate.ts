import { AuthApiPayload, AuthApiResult } from '../../api/defs/authenticate';

type AuthenticateClientApi = {
    post: (payload: AuthApiPayload) => Promise<AuthApiResult>;
};

export default AuthenticateClientApi;
