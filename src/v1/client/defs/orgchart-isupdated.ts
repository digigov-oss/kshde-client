type OrgchartIsUpdatedClientApi = {
    get: () => Promise<boolean>;
};
export default OrgchartIsUpdatedClientApi;
