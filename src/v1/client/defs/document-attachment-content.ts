import { Readable } from 'stream';

type DocumentAttachmentContentClientApi = {
    get: () => Promise<Readable>;
};
export default DocumentAttachmentContentClientApi;
