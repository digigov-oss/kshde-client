import OrgchartIsUpdatedClientApi from './orgchart-isupdated';

type OrgchartVersionClientApi = {
    updated: OrgchartIsUpdatedClientApi;
};

export default OrgchartVersionClientApi;
