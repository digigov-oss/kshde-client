import {
    GetReceiptsQueryParams,
    ReceiptApiResult,
    ReceiptCreateData
} from '../../api/defs/receipt';
import NavigatableApiResult from '../../api/utils/navigatable-api-result';
import ReceiptClientApi from './receipt';

type ReceiptsClientApi = {
    (receiptId: string): ReceiptClientApi;
    get: (queryParams?: GetReceiptsQueryParams) => Promise<NavigatableApiResult<ReceiptApiResult>>;
    post: (receipt: ReceiptCreateData) => Promise<ReceiptApiResult>;
};

export default ReceiptsClientApi;
