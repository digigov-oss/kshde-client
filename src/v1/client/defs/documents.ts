import { ReceiptApiResult } from '../../api/defs/receipt';
import {
    DocumentApiResult,
    DocumentCreateData,
    DocumentPayload,
    GetDocumentsQueryParams
} from '../../api/defs';
import NavigatableApiResult from '../../api/utils/navigatable-api-result';
import DocumentClientApi from './document';

type DocumentsClientApi = {
    (documentProtocolNo: string): DocumentClientApi;
    get: (
        queryParams?: GetDocumentsQueryParams
    ) => Promise<NavigatableApiResult<DocumentApiResult>>;
    post: (document: DocumentPayload<DocumentCreateData>) => Promise<ReceiptApiResult>;
};
export default DocumentsClientApi;
