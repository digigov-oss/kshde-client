import { GetDocumentContentQueryParams } from 'src/v1/api/defs';
import DocumentContentEndpoint from '../api/document-content';
import DocumentContentClientApi from './defs/document-content';

const createDocumentContentClientApi = (
    documentProtocolNo: string,
    documentContentEndpoint: DocumentContentEndpoint
): DocumentContentClientApi => {
    const urlParams = { documentProtocolNo: documentProtocolNo };
    return {
        get: (queryParams?: GetDocumentContentQueryParams) => {
            return documentContentEndpoint.get(urlParams, queryParams);
        }
    };
};
export default createDocumentContentClientApi;
