import OrgchartIsUpdatedEndpoint from '../api/orgchart-isupdated';
import OrgchartVersionClientApi from './defs/orgchart-version';
import createOrgchartIsUpdatedClientApi from './orgchart-isupdated';

const createOrgchartVersionClientApi = (
    currentVersion: number,
    orgchartIsUpdated: OrgchartIsUpdatedEndpoint
): OrgchartVersionClientApi => {
    return {
        updated: createOrgchartIsUpdatedClientApi(currentVersion, orgchartIsUpdated)
    };
};

export default createOrgchartVersionClientApi;
