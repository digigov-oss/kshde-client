import { OrgchartNode } from '../../api/defs/orgchart';
import OrgchartClientApi from '../defs/orgchart';

export interface FlatOrgchartNode extends OrgchartNode {
    ParentCode?: number;
}

class OrgchartUtils {
    private orgchart: OrgchartClientApi;

    constructor(orgchart: OrgchartClientApi) {
        this.orgchart = orgchart;
    }

    toArray = async (): Promise<FlatOrgchartNode[]> => {
        const response = await this.orgchart.get();
        return flattenOrgChart(response.RootNode.ChildNodes);
    };
}

function flattenOrgChart(childNodes?: OrgchartNode[]): FlatOrgchartNode[] {
    if (!childNodes) {
        return [];
    }
    return childNodes.reduce((acc: FlatOrgchartNode[], node: OrgchartNode) => {
        const { ChildNodes, ...rest } = node;
        return [
            ...acc,
            rest,
            ...flattenOrgChart(ChildNodes).map((n: any) => ({
                ...n,
                ParentCode: rest.Code
            }))
        ];
    }, []);
}

export default OrgchartUtils;
