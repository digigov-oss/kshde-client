import DocumentsClientApi from '../defs/documents';
import OrgchartClientApi from '../defs/orgchart';
import DocumentsUtils from './documents-utils';
import OrgchartUtils from './orgchart-utils';

class Utils {
    public documents: DocumentsUtils;
    public orgchart: OrgchartUtils;

    constructor(orgchart: OrgchartClientApi, documents: DocumentsClientApi) {
        this.documents = new DocumentsUtils(documents);
        this.orgchart = new OrgchartUtils(orgchart);
    }
}

export default Utils;
