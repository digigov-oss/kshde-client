import { validateFiles } from '../../utils/validateFiles';
import { DocumentCreateData, DocumentPayload } from '../../api/defs/document';
import {
    DocumentAttachmentApiResult,
    DocumentAttachmentCreateData
} from '../../api/defs/document-attachment';
import { ReceiptApiResult } from '../../api/defs/receipt';
import DocumentsClientApi from '../defs/documents';

export interface PostDocumentResult {
    receipt: ReceiptApiResult;
    attachments: DocumentAttachmentApiResult[];
}

export interface PostDocumentMetadata
    extends Omit<DocumentCreateData, 'IsFinal' | 'OrgChartVersion'> {
    OrgChartVersion?: number;
}

export interface PostAttachmentMetadata extends Omit<DocumentAttachmentCreateData, 'IsFinal'> {
    IsFinal?: boolean;
}

class DocumentsUtils {
    private documents: DocumentsClientApi;

    constructor(documents: DocumentsClientApi) {
        this.documents = documents;
    }

    postAll = async (
        document: DocumentPayload<PostDocumentMetadata>,
        attachments: DocumentPayload<PostAttachmentMetadata>[] = []
    ): Promise<PostDocumentResult> => {
        validateFiles(
            document.metadata,
            attachments.map((attachment) => attachment.metadata)
        );
        const documentMetadata: DocumentCreateData = {
            ...document.metadata,
            IsFinal: attachments.length === 0,
            OrgChartVersion: document.metadata.OrgChartVersion ?? 1
        };
        const receipt = await this.documents.post({
            data: document.data,
            metadata: documentMetadata
        });
        const result: PostDocumentResult = { receipt, attachments: [] };
        let success = isSuccessResult(receipt);
        if ('DocumentProtocolNo' in receipt && attachments.length > 0) {
            const documentProtocolNo = receipt.DocumentProtocolNo;
            for (let i = 0; i < attachments.length; i++) {
                const attachment = attachments[i];
                const isFinal = success && i === attachments.length - 1 ? true : false;
                const attachmentPayload: DocumentPayload<DocumentAttachmentCreateData> = {
                    data: attachment.data,
                    metadata: {
                        FileName: attachment.metadata.FileName,
                        Comments: attachment.metadata.Comments,
                        IsFinal: isFinal
                    }
                };
                const attachmentResp =
                    await this.documents(documentProtocolNo).attachments.post(attachmentPayload);
                result.attachments.push(attachmentResp);
                success = success && isSuccessResult(attachmentResp);
            }
        }
        if (!success) {
            throw result;
        }
        return result;
    };
}

function isSuccessResult(response: ReceiptApiResult | DocumentAttachmentApiResult) {
    return !('ErrorCode' in response);
}

export default DocumentsUtils;
