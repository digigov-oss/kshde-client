import NotificationsEndpoint from '../api/notifications';
import { GetNotificationsQueryParams } from '../api/defs/notifications';
import NotificationsClientApi from './defs/notifications';
import EndpointUrl from '../api/defs/endpoint-url';
import { ResourceProps } from '../api/utils/resource';

const createNotificationsClientApi = (resourceProps: ResourceProps): NotificationsClientApi => {
    const notifications = new NotificationsEndpoint(resourceProps, EndpointUrl.NOTIFICATIONS);
    return {
        get: (queryParams?: GetNotificationsQueryParams) => {
            return notifications.get({}, queryParams);
        }
    };
};
export default createNotificationsClientApi;
