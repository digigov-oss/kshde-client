interface NamedFile {
    FileName: string;
}

export function validateFileName(fileName: string) {
    if (!isFilenameValid(fileName)) {
        throw new Error(
            `FileName ${fileName} contains special characters or starts or ends with dot or is longer than 255 characters`
        );
    }
}
export function validateFiles(document: NamedFile, attachments?: NamedFile[]): void {
    const fileNames: string[] = [];

    function validateFile(file: NamedFile) {
        if (file.FileName) {
            if (fileNames.includes(file.FileName))
                throw new Error(`FileName ${file.FileName} is not unique`);
            validateFileName(file.FileName);
            fileNames.push(file.FileName);
        }
    }

    validateFile(document);
    attachments?.forEach((attachment) => validateFile(attachment));
}

export function isFilenameValid(filename: string) {
    //No dot at begining or end.
    //No ;/:*%<>|" anywhere
    return !/^[.]|[;/:*%<>|"]|[.]$/.test(filename) && filename.length <= 255;
}
